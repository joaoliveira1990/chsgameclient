# Installation
 
This project uses .NET Framework 4.7.1 Developer Pack - [Microsoft Download](https://dotnet.microsoft.com/download/dotnet-framework/net471).
 
 # Usage

 # Development

Standard IDE for this project is VSCode, but any other IDE suitable for C# projects are also viable.

## VSCode Configuration
 
Settings.json file
 
```
{
    "tabnine.experimentalAutoImports": true,
    "files.exclude": {
        "**/*.meta": true
      },
}
```

## Notation

Regarding C# scripts, there are some convention we try to follow:

- Variable names
  - class attribute with `this` (example: `this.someVar`)
  - function/method parameter with `_` before (example: `_someVar`)
  - local variable with no changes to its name

# Dependencies

Unity dependencies are installed via the Asset Store. They are mostly organized in the `External Assets` folder.
### RPG and MMO UI 11

This dependency needs some changes to work properly with Unity:

- In folder `Scripts` change name of folders `Editor` -> `EditorCode`
- In all scripts inside `EditorCode`, wrap the WHOLE code in:

```
#if UNITY_EDITOR

(code)

#endif
```

In general, this asset is being directly edited in `Resources/Databases`, so backup (outside of unity) `Databases`.