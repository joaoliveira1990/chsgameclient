﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Treasure : MonoBehaviour
{    
    [SerializeField] private Transform m_Slot;
    [SerializeField] private Text coinValueText;
    private Animator animator;
    private bool isOpen = false;

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("Clicked: " + eventData.pointerCurrentRaycast.gameObject.name);
    }

    private void OnMouseEnter() 
    {
        Debug.Log("OnMouseEnter");
        animator = m_Slot.GetChild(0).GetComponent<Animator>();
        if (animator)
        {
            animator.Play("Chest _Idle");
        }
    }
    private void OnMouseExit() 
    {
        Debug.Log("OnMouseExit");
        animator = m_Slot.GetChild(0).GetComponent<Animator>();
        if (animator)
        {
            animator.StopPlayback();
        }
    }
    private void OnMouseDown() 
    {
        Debug.Log("OnMouseDown");
        animator = m_Slot.GetChild(0).GetComponent<Animator>();
        if (animator)
        {
            if (isOpen)
            {
                animator.Play("Chest _Press");
            }
            else
            {
                animator.Play("Chest _Opening_Common");
                this.isOpen = true;
            }
        }

        switch (m_Slot.name)
        {
            case "Slot (3)":
            coinValueText.text = "100";
            break;
            case "Slot (1)":
            coinValueText.text = "250";
            break;
            case "Slot (2)":
            coinValueText.text = "500";
            break;
        }
    }
}
