﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipWeaponSkins : MonoBehaviour
{
    public GameObject[] spec1WeaponSet1 = {};
    public GameObject[] spec1WeaponSet2 = {};
    public GameObject[] spec1WeaponSet3 = {};

    public GameObject[] spec2WeaponSet1 = {};
    public GameObject[] spec2WeaponSet2 = {};
    public GameObject[] spec2WeaponSet3 = {};

    public GameObject[] spec3WeaponSet1 = {};
    public GameObject[] spec3WeaponSet2 = {};
    public GameObject[] spec3WeaponSet3 = {};

    private List<GameObject[]> spec1WeaponSets = new List<GameObject[]>();
    private List<GameObject[]> spec2WeaponSets = new List<GameObject[]>();
    private List<GameObject[]> spec3WeaponSets = new List<GameObject[]>();
    private List<GameObject[]> selectedSpec = new List<GameObject[]>();
    private int selectedWeaponSetIndex = 0;
    private GameObject[] selectedWeaponSet = {};

    public void Initialize()
    {
        RemoveWeaponSets();
        AddWeaponSets();

        selectedSpec = this.spec1WeaponSets;
    }

    private void RemoveWeaponSets()
    {
        if (spec1WeaponSets == null && spec2WeaponSets == null && spec3WeaponSets == null) return;
        this.spec1WeaponSets.Clear();
        this.spec1WeaponSets.Clear();
        this.spec1WeaponSets.Clear();

        this.spec2WeaponSets.Clear();
        this.spec2WeaponSets.Clear();
        this.spec2WeaponSets.Clear();

        this.spec3WeaponSets.Clear();
        this.spec3WeaponSets.Clear();
        this.spec3WeaponSets.Clear();
    }

    private void AddWeaponSets()
    {
        this.spec1WeaponSets.Add(this.spec1WeaponSet1);
        this.spec1WeaponSets.Add(this.spec1WeaponSet2);
        this.spec1WeaponSets.Add(this.spec1WeaponSet3);

        this.spec2WeaponSets.Add(this.spec2WeaponSet1);
        this.spec2WeaponSets.Add(this.spec2WeaponSet2);
        this.spec2WeaponSets.Add(this.spec2WeaponSet3);

        this.spec3WeaponSets.Add(this.spec3WeaponSet1);
        this.spec3WeaponSets.Add(this.spec3WeaponSet2);
        this.spec3WeaponSets.Add(this.spec3WeaponSet3);
    }
    public void OnPressSpecButton(int specNumber) 
    {
        this.Initialize();
        Debug.Log($"PRESSED SPEC BTN {specNumber}");
        if(selectedWeaponSet.Length > 0) ToggleObjects(selectedWeaponSet, false);
        switch (specNumber)
        {
            case 1:
                selectedSpec = this.spec1WeaponSets;
                selectedWeaponSet = this.spec1WeaponSets[selectedWeaponSetIndex];
                ToggleObjects(selectedWeaponSet, true);
            break;
            case 2:
                selectedSpec = this.spec2WeaponSets;
                selectedWeaponSet = this.spec2WeaponSets[selectedWeaponSetIndex];
                ToggleObjects(selectedWeaponSet, true);
            break;
            case 3:
                selectedSpec = this.spec3WeaponSets;
                selectedWeaponSet = this.spec3WeaponSets[selectedWeaponSetIndex];
                ToggleObjects(selectedWeaponSet, true);
            break;
        }
    }

    public void EquipSet(List<GameObject[]> _selectedSpec)
    {
        ToggleObjects(selectedWeaponSet, false);
        selectedWeaponSet = _selectedSpec[selectedWeaponSetIndex];
        ToggleObjects(selectedWeaponSet, true);
    }

    public int EquipNextWeaponSet()
    {
        if(selectedSpec.Count > 0)
        {
            selectedWeaponSetIndex++;

            if(selectedWeaponSetIndex >= selectedSpec.Count)
            {
                selectedWeaponSetIndex = 0;
            }
            EquipSet(selectedSpec);
        }
        return selectedWeaponSetIndex;
    }

    public int EquipPreviousWeaponSet()
    {
        if(selectedSpec.Count > 0)
        {
            selectedWeaponSetIndex--;

            if(selectedWeaponSetIndex < 0)
            {
                selectedWeaponSetIndex = selectedSpec.Count - 1;
            }
            EquipSet(selectedSpec);
        }
        return selectedWeaponSetIndex;
    }

    private void ToggleObjects(GameObject[] objs, bool activate)
    {
        for (int i = 0; i < objs.Length; i++)
        {
            objs[i].SetActive(activate);
        }
    }
}
