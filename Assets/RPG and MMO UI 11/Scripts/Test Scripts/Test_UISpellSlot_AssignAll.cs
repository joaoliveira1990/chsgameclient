using UnityEngine;
using System.Collections.Generic;
namespace DuloGames.UI
{
    public class Test_UISpellSlot_AssignAll : MonoBehaviour
    {
        #pragma warning disable 0649
        [SerializeField] private Transform m_Container;
        #pragma warning restore 0649

        void Start()
        {
            if (this.m_Container == null || UISpellDatabase.Instance == null)
            {
                this.Destruct();
                return;
            }

            UpdateTabs();

            //this.Destruct();
        }

        private void Destruct()
        {
            DestroyImmediate(this);
        }

        public void UpdateTabs()
        {
            UISpellInfo_Specs selectedSpec = GameManager.instance.playerCamInstance.GetComponent<HudController>().GetSelectedSpec();
            UISpellSlot[] slots = this.m_Container.gameObject.GetComponentsInChildren<UISpellSlot>();
            
            List<UISpellInfo> spells = null;

            if (this.transform.parent.name == "Content (Talents)")
            {
                spells = UISpellDatabase.Instance.GetBySpecAndType(selectedSpec, UISpellInfo_Type.Talent);
            }
            else
            {
                spells = TalentManager.instance.GetSelectedTalents();
            }
            //List<UISpellInfo> spells = UISpellDatabase.Instance.GetBySpecAndType(selectedSpec, UISpellInfo_Type.Talent);

            if (slots.Length > 0 && spells.Count > 0)
            {
                int spellsIndex = 0;
                foreach (UISpellSlot slot in slots)
                {
                    if (spellsIndex < spells.Count)
                    {
                        slot.Assign(spells[spellsIndex]);
                        spellsIndex++;
                        //TODO add spell info to ui
                    }
                }
            }
        }
    }
}
