using UnityEngine;
using System;

namespace DuloGames.UI
{
	[Serializable]
	public class UIItemInfo
	{
		public int ID;
		public string Name;
		public Sprite Icon;
		public string Description;
        public UIItemQuality Quality;
        public UIEquipmentType EquipType;
		public int ItemType;
		public string Type;
		public string Subtype;
		public int AttackDamage;
		public int SpellDamage;
		public float Haste;
		public int MagicResist;
		public int Armor;
		public int Stamina;
		public int Strength;
		public int Agility;
		public int Intellect;
        public int Mana;
		public int HpRegen;
		public int ManaRegen;
		public int Crit;
        public int RequiredLevel;
		public int GoldValue;
	}
}
