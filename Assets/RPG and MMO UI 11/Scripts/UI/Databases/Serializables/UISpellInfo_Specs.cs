using System;

namespace DuloGames.UI
{
    public enum UISpellInfo_Specs
    {
        Fury,
        Arms,
        Prot,
        Fire,
        Frost,
        Arcane,
        Holy,
        Discipline,
        Shadow,
        Inactive
    }

    public enum UISpellInfo_Type
    {
        Basic,
        Talent
    }
}
