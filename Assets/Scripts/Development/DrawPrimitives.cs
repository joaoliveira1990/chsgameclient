﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawPrimitives : MonoBehaviour
{
    public static DrawPrimitives instance;
    [SerializeField] bool isOn = true;
    [SerializeField] float time = 5f;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }
    }

    public void DrawBall(Vector3 _position)
    {
        if (this.isOn)
        {
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            Renderer render = sphere.GetComponent<Renderer>();
            Material material = sphere.GetComponent<Material>();

            sphere.transform.position = _position;
            sphere.transform.localScale = new Vector3(1, 1, 1);
            sphere.layer = 2;

            render.material = new Material(Shader.Find("Specular"));

            StartCoroutine(DestroyAfter(sphere));
        }
    }

    public void DrawVerticalRay(Vector3 _newPosition, float _height)
    {
        if (this.isOn)
        {
            Vector3 verticalRay = _newPosition;
            verticalRay.y = _height;

            Debug.DrawRay(verticalRay, new Vector3(0f, -100f, 0f), Color.green, this.time);
        }
    }

    public void DrawRay(Vector3 _newPosition, Vector3 _direction)
    {
        if (this.isOn)
        {
            Debug.DrawRay(_newPosition, _direction, Color.red, this.time);
        }
    }

    // * Internals

    private IEnumerator DestroyAfter(GameObject _gameObject)
    {
        yield return new WaitForSeconds(this.time);
        Destroy(_gameObject);
    }
}
