﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DuloGames.UI;
public class Shop : MonoBehaviour
{
    [SerializeField] private GameObject highlight;
    private void OnMouseDown() 
    {
        GameManager.instance.playerCamInstance.GetComponent<HudController>().OpenShopWindow();
    }

    private void OnMouseOver() 
    {
        highlight.SetActive(true);
    }

    private void OnMouseExit() 
    {
        highlight.SetActive(false);
    }
}
