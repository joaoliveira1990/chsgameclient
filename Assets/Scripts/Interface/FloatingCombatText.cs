﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingCombatText : MonoBehaviour
{

    float destroyTime = 0.6f;
    private Camera playerCam;
    public Vector3 offSet = new Vector3(0, 4, 0);
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, destroyTime);
        transform.localPosition += offSet;
    }

    private void Update()
    {

        if (GameManager.instance.localPlayerId > 0)
        {
            PlayerManager localPlayer = GameManager.players[GameManager.instance.localPlayerId];
            if (GameManager.instance.isCameraSet())
            {
                playerCam = GameManager.instance.playerCamInstance;

                transform.LookAt(transform.position + playerCam.transform.forward);
            }
        }
    }
}
