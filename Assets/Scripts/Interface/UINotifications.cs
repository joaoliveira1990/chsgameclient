﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UINotifications : MonoBehaviour
{

    [SerializeField] private GameObject objTohide;
    public void Show()
    {
        objTohide.SetActive(false);
        this.gameObject.SetActive(true);
    }

    private void OnMouseDown() 
    {
        objTohide.SetActive(true);
        this.gameObject.SetActive(false);
    }
}
