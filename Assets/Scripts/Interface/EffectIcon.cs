﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using System;
using UnityEngine.UI;

public class EffectIcon : MonoBehaviour
{
  [SerializeField] public AllEffects id;
  [SerializeField] public Text stackCount = null;
  [SerializeField] public Text lifeTimeCount;
}
