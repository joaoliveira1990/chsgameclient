﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>Handle character highlights and mouse icons on hover</summary> 
public class ColliderOnOver : MonoBehaviour
{
    private bool IsOver = false;
    [SerializeField] private Material highlightMaterialAlly = null;
    [SerializeField] private Material highlightMaterialEnemy = null;
    [SerializeField] private Texture2D cursorAttack = null;
    [SerializeField] private Texture2D cursorSupport = null;
    [SerializeField] private Renderer[] meshRenderers = { };
    private Texture2D cursorNormal = null;

    private void Start()
    {
        // TODO: Get from a DataResource file
        if (this.highlightMaterialAlly == null) this.highlightMaterialAlly = Resources.Load<Material>("OnOverEffects/outline-highlight-ally");
        if (this.highlightMaterialEnemy == null) this.highlightMaterialEnemy = Resources.Load<Material>("OnOverEffects/outline-highlight-enemy");
        if (this.cursorAttack == null) this.cursorAttack = Resources.Load<Texture2D>("OnOverEffects/cursor-attack");
        if (this.cursorSupport == null) this.cursorSupport = Resources.Load<Texture2D>("OnOverEffects/cursor-support");
    }

    private void OnMouseOver()
    {
        if (this.tag == "Enemy")
        {
            // Switch cursor mode to loot here in the future
            if (this.GetComponentInParent<EnemyManager>().isDead)
            {
            }
        }
    }

    private void OnMouseEnter()
    {
        IsOver = true;
        for (int i = 0; i < meshRenderers.Length; i++)
        {
            Material[] materials = this.meshRenderers[i].materials;
            Material[] intMaterials = new Material[materials.Length + 1];

            for (int o = 0; o < intMaterials.Length - 1; o++)
            {
                intMaterials[o] = materials[o];
            }

            // Change Cursor and highlight
            if (this.tag == "Player")
            {
                if (GameManager.instance.team == this.GetComponentInParent<PlayerManager>().team)
                {
                    Cursor.SetCursor(this.cursorSupport, Vector3.zero, CursorMode.Auto);
                    GameManager.instance.mouseMode = "support";
                    intMaterials[intMaterials.Length - 1] = this.highlightMaterialAlly;
                }
                else
                {
                    Cursor.SetCursor(this.cursorAttack, Vector3.zero, CursorMode.Auto);
                    GameManager.instance.mouseMode = "attack";
                    intMaterials[intMaterials.Length - 1] = this.highlightMaterialEnemy;
                }
                // TODO: Check player team
            }
            else
            {
                Cursor.SetCursor(this.cursorAttack, Vector3.zero, CursorMode.Auto);
                GameManager.instance.mouseMode = "attack";
                intMaterials[intMaterials.Length - 1] = this.highlightMaterialEnemy;
            }

            this.meshRenderers[i].materials = intMaterials;
        }
    }

    private void OnMouseExit()
    {
        IsOver = false;
        Cursor.SetCursor(this.cursorNormal, Vector3.zero, CursorMode.Auto);
        GameManager.instance.mouseMode = "none";

        for (int i = 0; i < meshRenderers.Length; i++)
        {
            Material[] materials = this.meshRenderers[i].materials;
            Material[] intMaterials = new Material[materials.Length - 1];

            for (int o = 0; o < intMaterials.Length; o++)
            {
                intMaterials[o] = materials[o];
            }

            this.meshRenderers[i].materials = intMaterials;
        }
    }

    public void ForceExit()
    {
        //target died while on over
        Cursor.SetCursor(this.cursorNormal, Vector3.zero, CursorMode.Auto);
        GameManager.instance.mouseMode = "none";
        this.enabled = false;
    }
}
