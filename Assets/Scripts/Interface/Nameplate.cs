﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nameplate : MonoBehaviour
{
    [SerializeField] private Transform parent;
    // Start is called before the first frame update
    void Start()
    {
      this.transform.position = Camera.main.WorldToScreenPoint(parent.position);
    }

    // Update is called once per frame
    void Update()
    {
      this.transform.position = Camera.main.WorldToScreenPoint(parent.position);
    }
}
