﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DuloGames.UI;
using System.Linq;

public class TalentManager : MonoBehaviour
{
    public static TalentManager instance;
    private List<UISpellInfo> selectedTalents = new List<UISpellInfo>(); 
    [SerializeField] private GameObject spellSlots;
    [SerializeField] private GameObject talentSlots; 
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }
    }

    public void AddTalent(Toggle toggle)
    {
        if (toggle.isOn)
        {
            Toggle[] toggles = toggle.group.gameObject.GetComponentsInChildren<Toggle>();
            foreach (Toggle tog in toggles)
            {
                tog.interactable = false;
            }
            UISpellInfo spell = toggle.gameObject.transform.GetChild(0).GetComponent<UISpellSlot>().GetSpellInfo();
            selectedTalents.Add(spell);
            int playerId = GameManager.instance.localPlayerId;
            ClientSend.AddTalent(playerId, spell.ID);
        }
    }

    public void TabChanged(Toggle toggle)
    {
        if (toggle.isOn)
        {
            Debug.Log($"{toggle.name}");
            if (toggle.name == "Tab Button (1)")
            {
                spellSlots.GetComponent<Test_UISpellSlot_AssignAll>().UpdateTabs();
            }
            else
            {

            }
        }
    }

    public List<UISpellInfo> GetSelectedTalents()
    {
        return this.selectedTalents;
    }
}
