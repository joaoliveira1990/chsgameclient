﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EffectIcon1 : MonoBehaviour
{
    private float lifeTime;
    private int stacks = 1;
    public int maxStacks = 1;
    private float remainingDuration;

    public AllEffects effectName;
    public TMP_Text stackCount;
    public TMP_Text lifeTimeCount;

    private GameObject updater;

    public void Initialize(float _lifeTime, GameObject _updater)
    {
        this.lifeTime = _lifeTime;
        this.remainingDuration = _lifeTime;
        this.updater = _updater;

        StopAllCoroutines();
        //IEnumerator updateLifeTime = UpdateLifeTime(lifeTimeCount);
        StartCoroutine("UpdateLifeTime");
        StartCoroutine("DestroyAfter");
    }

    private IEnumerator UpdateLifeTime()
    {
        yield return new WaitForSeconds(1f);
        remainingDuration -= 1;
        lifeTimeCount.SetText(remainingDuration + "");
        if (remainingDuration < 0)
        {
            StopCoroutine("UpdateLifeTime");
        }
        StartCoroutine("UpdateLifeTime");
    }

    private IEnumerator DestroyAfter()
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(this.gameObject);
        updater.GetComponent<HUDUpdater>().CleanBuffs();
    }

    public void DestroyEffect()
    {
        Destroy(this.gameObject);
    }

    public void AddStack()
    {
        StopCoroutine("DestroyAfter");
        StopCoroutine("UpdateLifeTime");
        if (stacks < maxStacks) stacks += 1;
        remainingDuration = lifeTime;
        lifeTimeCount.SetText(remainingDuration + "");
        stackCount.SetText(stacks + "");
        StartCoroutine("DestroyAfter");
        StartCoroutine("UpdateLifeTime");
    }
}
