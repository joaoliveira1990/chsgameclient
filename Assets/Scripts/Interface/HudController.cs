﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DuloGames.UI;
using UnityEngine.UI;
using System;

public class HudController : MonoBehaviour
{
    [SerializeField] private UIProgressBar healthProgressBar;
    [SerializeField] private UIProgressBar resourceProgressBar;
    [SerializeField] private GameObject effectsBar;
    [SerializeField] private GameObject effectIcon;
    [SerializeField] private GameObject[] effects;
    [NonSerialized] private List<GameObject> currentEffects = new List<GameObject>();


    [SerializeField] private UISpellSlot[] slots;
    [SerializeField] private UISpellInfo_Specs[] warriorSpecs;
    [SerializeField] private UISpellInfo_Specs[] mageSpecs;
    [SerializeField] private UISpellInfo_Specs[] priestSpecs;
    [SerializeField] private GameObject inventoryWindow;
    [SerializeField] private Transform inventorySlotsContainer;
    [SerializeField] private GameObject shopWindow;
    [SerializeField] private Text goldText;
    [SerializeField] private UIProgressBar xpBar;
    [SerializeField] private Text levelText;
    [SerializeField] Text strengthValue;
    [SerializeField] Text intellectValue;
    [SerializeField] Text agilityValue;
    [SerializeField] Text staminaValue;
    [SerializeField] Text armorValue;
    [SerializeField] Text magicResistanceValue;
    [SerializeField] Text healthPer5Value;
    [SerializeField] Text manaPer5Value;
    [SerializeField] Text damagePerSecondValue;
    [SerializeField] Text attackSpeedValue;
    [SerializeField] Text critChanceValue;
    [SerializeField] Text physicalDamageValue;
    [SerializeField] Text spellDamageValue;
    private UISpellInfo_Specs spec;
    private List<UISpellInfo> baseSpells;
    private int playerId = 0;
    private bool isOnGlobalCooldown = false;


    void Start()
    {
        PlayerManager player = GameManager.instance.localPlayer.GetComponent<PlayerManager>();
        GameManager.instance.localPlayerId = player.id; //??

        SetPlayerId(player.id);
        SetPlayerSpec(player.selectedClass, player.selectedSpec);
        AssignBaseAbilities();
    }
    void Update()
    {
        checkKeyPresses();
    }

    public void SetPlayerId(int _id)
    {
        playerId = _id;
    }

    public void SetPlayerSpec(int _selectedClass, int _selectedSpec)
    {
        switch (_selectedClass)
        {
            case 1:
                spec = warriorSpecs[_selectedSpec - 1];
                break;
            case 2:
                spec = mageSpecs[_selectedSpec - 1];
                break;
            case 3:
                spec = priestSpecs[_selectedSpec - 1];
                break;
            default:
                break;
        }
        baseSpells = UISpellDatabase.Instance.GetBySpecAndType(spec, UISpellInfo_Type.Basic);
    }

    public UISpellInfo_Specs GetSelectedSpec()
    {
        return this.spec;
    }

    private float SetValueDecimal(float _value, float _maxValue)
    {
        if (_value > _maxValue) return 1f;
        return _value / _maxValue;
    }

    public void AssignBaseAbilities()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (i < baseSpells.Count)
            {
                this.slots[i].Assign(baseSpells[i]);
            }
        }
    }
    private void checkKeyPresses()
    {
        int input = Mappings.InputToNumber(Input.inputString);
        if (input > -1)
        {
            this.slots[input].onClick.Invoke(this.slots[input]);
        }
    }

    public void SetHealthBarValue(float _health, float _maxHealth)
    {
        if (this.healthProgressBar == null) return;
        this.healthProgressBar.fillAmount = SetValueDecimal(_health, _maxHealth);
    }

    public void SetResourceBarValue(float _value, float _maxValue)
    {
        if (this.resourceProgressBar == null) return;
        this.resourceProgressBar.fillAmount = SetValueDecimal(_value, _maxValue);
    }

    public void OnPressHotbarButtons(UISpellSlot slot)
    {
        if (isOnGlobalCooldown) return;
        //int buttonNumber = int.Parse(button.name[button.name.Length - 1] + "");
        UISpellInfo spellInfo = slot.GetSpellInfo();
        if (spellInfo == null) return;
        int spellId = slot.GetSpellInfo().ID;
        Vector3 mouseTarget = Vector3.zero;
        Ray ray = this.GetComponent<Camera>().ScreenPointToRay(pos: Input.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            mouseTarget = hit.point;
        }

        ClientSend.TriggerAbility(playerId, spellId, mouseTarget);
        StartCoroutine(SendCastCommandFixedTime(1f)); //"global cooldown ver implementacao"
    }

    private IEnumerator SendCastCommandFixedTime(float _delay)
    {
        isOnGlobalCooldown = true;
        yield return new WaitForSeconds(_delay);
        isOnGlobalCooldown = false;
    }

    public void AddEffect(int _effectId, float _lifeTime, int _stacks)
    {    
        if (IsOutOfBounds(this.effects, _effectId)) return;
        CleanBuffs();

        GameObject existingEffect = this.currentEffects.Find(e =>  (int)e.GetComponent<EffectIcon>().id == _effectId);
        if (existingEffect != null) Destroy(existingEffect);

        GameObject effect = this.effects[_effectId];
        GameObject effectInstance = Instantiate(effect, effectsBar.transform);
        EffectIcon effectIcon = effectInstance.GetComponent<EffectIcon>();
        effectIcon.stackCount.text = _stacks.ToString();
        effectIcon.stackCount.gameObject.SetActive(true);

        Destroy(effectInstance, _lifeTime);
        currentEffects.Add(effectInstance);
    }

    public void RemoveEffect(int _effectId)
    {
        if (IsOutOfBounds(this.effects, _effectId)) return;
        CleanBuffs();

        GameObject existingEffect = this.currentEffects.Find(e =>  (int)e.GetComponent<EffectIcon>().id == _effectId);
        if (existingEffect != null) Destroy(existingEffect);
    }

    // TODO: place this as a Misc util function (common pattern)
    public void CleanBuffs()
    {
        this.currentEffects.RemoveAll(e => e == null);
    }

    private bool IsOutOfBounds(GameObject[] _array, int _id)
    {
        return _id < 0 || _id >= _array.Length;
    }

    public void OnPressInventoryButton()
    {
        //this.inventoryWindow.SetActive(true);
        this.inventoryWindow.GetComponent<UIWindow>().Show();
    }

    public void OpenShopWindow()
    {
        this.shopWindow.GetComponent<UIWindow>().Show();
    }

    public void OnDoubleClickShopItem(UIItemSlot item)
    {
        Debug.Log("clicked item id: " + item.GetItemInfo().ID);
        ClientSend.BuyItem(playerId, item.GetItemInfo().ID);
    }

    public void SetGold(int _gold)
    {
        goldText.text = _gold+"";
    }

    public void SetXpLevel(int _level, int _currentXp, int _xpRequiredToLevel)
    {
        levelText.text = _level+"";
        Debug.Log($"CURRENT XP = {_currentXp}");
        Debug.Log($"REQUIRED XP = {_xpRequiredToLevel}");
        Debug.Log($"PERCENTAGE XP = {((_currentXp * 100f)/_xpRequiredToLevel)/100f}");
        xpBar.fillAmount = ((_currentXp * 100f)/_xpRequiredToLevel)/100f;
    }

    public void EquipItem(int _itemId, int _gold)
    {
        if (inventorySlotsContainer != null)
        {
            // Grab all the slots in the container
            UIItemSlot[] slots = inventorySlotsContainer.GetComponentsInChildren<UIItemSlot>();

            foreach (UIItemSlot slot in slots)
            {
                if (!slot.IsAssigned())
                {
                    slot.Assign(UIItemDatabase.Instance.GetByID(_itemId));
                    SetGold(_gold);
                    return;
                }
            }
        }
    }

    public void UpdateStats(Stats playerStats)
    {
        strengthValue.text = playerStats.strength+"";
        intellectValue.text = playerStats.intellect+"";
        agilityValue.text = playerStats.agility+"";
        staminaValue.text = playerStats.stamina+"";
        armorValue.text = playerStats.armor+"";
        magicResistanceValue.text = playerStats.magicResistance+"";
        healthPer5Value.text = playerStats.healthPer5+"";
        manaPer5Value.text = playerStats.manaPer5+"";
        damagePerSecondValue.text = (playerStats.physicalDamage*(60/playerStats.attackSpeed))/60+"";
        critChanceValue.text = playerStats.critChance+"";
        attackSpeedValue.text = playerStats.attackSpeed+"";
        physicalDamageValue.text = Math.Floor(playerStats.physicalDamage)+"";
        spellDamageValue.text = Math.Floor(playerStats.spellDamage)+"";
    } 
    
}
