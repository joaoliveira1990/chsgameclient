﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    public GameObject startMenu;
    public GameObject selectChampion;
    public InputField usernameField;
    public GameManager gameManager;
    public ToggleGroup teamToggles;

    public GameObject specPanel1;
    public GameObject specPanel2;
    public GameObject specPanel3;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            ClientLogging.instance.Debugger("INFO: Instance already exists, destroying objects");
            Destroy(this);
        }
    }

    public void ConnectToServer()
    {
        startMenu.SetActive(false);
        selectChampion.SetActive(false);
        usernameField.interactable = false;
        Client.instance.ConnectToServer();
    }


    //on clicks tudo no mesmo
    /*
    public void OnClickSelectArthur()
    {
        gameManager.selectedChampion = 0;
    }
    */

    public void OnHoverArthur()
    {
    }

    //on clicks tudo no mesmo
    /*
    public void OnClickSelectSguy()
    {
        gameManager.selectedChampion = 1;
    }
    */

    public void OnHoverSguy()
    {
    }

    public void OnSelectTeam()
    {
        Toggle toggle = teamToggles.ActiveToggles().FirstOrDefault();
        if (toggle.gameObject.name == "Toggle Team 1")
        {
            gameManager.team = 1;
        }
        else
        {
            gameManager.team = 2;
        }
    }

    public void OnClickClassButtons(Button button)
    {
        specPanel1.SetActive(false);
        specPanel2.SetActive(false);
        specPanel3.SetActive(false);

        switch (button.name)
        {
            case "Class1":
                specPanel1.SetActive(true);
                gameManager.selectedClass = 1;
                break;
            case "Class2":
                specPanel2.SetActive(true);
                gameManager.selectedClass = 2;
                break;
            case "Class3":
                specPanel3.SetActive(true);
                gameManager.selectedClass = 3;
                break;
            default:
                break;
        }
    }

    public void OnClickSpecButtons(Button button)
    {
        switch (button.name)
        {
            case "Spec1":
                gameManager.selectedSpec = 1;
                break;
            case "Spec2":
                gameManager.selectedSpec = 2;
                break;
            case "Spec3":
                gameManager.selectedSpec = 3;
                break;
            default:
                break;
        }
    }
}
