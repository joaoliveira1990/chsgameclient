﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class HUDUpdater : MonoBehaviour
{
    public Text healthCounter;
    public Text time;
    public GameObject teamColor;

    public GameObject[] warriorHotbars;
    public GameObject[] mageHotbars;
    public GameObject[] priestHotbars;
    private GameObject hotbar = null;

    public GameObject effectsBar;
    public GameObject[] effects;
    private List<GameObject> currentEffects = new List<GameObject>();

    public TextMesh floatingCombatText;

    private List<Button> abilityButtons = new List<Button>();
    public ResourceSlider healthSlider;
    public ResourceSlider resourceSlider;
    public float clockTimeLapse = 0.0f;
    public float clockPeriodUpdate = 1.0f;
    public System.DateTime startTime = System.DateTime.UtcNow;
    private bool isOnGlobalCooldown = false;

    private int playerId = 0;

    public void Start()
    {
        PlayerManager player = GameManager.instance.localPlayer.GetComponent<PlayerManager>();

        // - Team
        int team = GameManager.instance.team;
        teamColor.GetComponent<Image>().color = Mappings.TeamToColor(team);

        // - Resource color
        int selectedClass = GameManager.instance.localPlayer.GetComponent<PlayerManager>().selectedClass;
        resourceSlider.SetColor(Mappings.ClassToColor(selectedClass));

        GameManager.instance.localPlayerId = player.id;

        SetPlayerId(player.id);
        SetHotbar(player.selectedClass, player.selectedSpec);
        SetResourceBar(player.selectedClass);
    }

    public void Update()
    {
        checkKeyPresses();

        // - Update clock
        if (this.clockTimeLapse > this.clockPeriodUpdate)
        {
            System.TimeSpan ts = System.DateTime.UtcNow - startTime;
            SetTimerValue(ts);
            this.clockTimeLapse = 0;
        }
        this.clockTimeLapse += UnityEngine.Time.deltaTime;
    }

    public void SetResourceBar(int _selectedClass)
    {

        switch (_selectedClass)
        {
            case 1:
                GameManager.instance.localPlayer.transform.Find("RagePlate").gameObject.SetActive(true);
                break;
            case 2:
                //GameManager.instance.localPlayer.transform.Find("ManaPlate").gameObject.SetActive(true);
                break;
            case 3:
                //GameManager.instance.localPlayer.transform.Find("ManaPlate").gameObject.SetActive(true);
                break;
            default:
                break;
        }
    }

    private void checkKeyPresses()
    {
        int input = Mappings.InputToNumber(Input.inputString);
        if (input > -1)
        {
            abilityButtons[input].onClick.Invoke();
        }
    }

    public void SetHotbar(int _selectedClass, int _selectedSpec)
    {
        //GameObject hotbar = null;

        switch (_selectedClass)
        {
            case 1:
                hotbar = warriorHotbars[_selectedSpec - 1];
                break;
            case 2:
                hotbar = mageHotbars[_selectedSpec - 1];
                break;
            case 3:
                hotbar = priestHotbars[_selectedSpec - 1];
                break;
            default:
                break;
        }
        hotbar.SetActive(true);

        for (int i = 0; i < hotbar.transform.childCount; i++)
        {
            abilityButtons.Add(hotbar.transform.GetChild(i).GetComponent<Button>());
        }
    }

    public void ResetTimer()
    {
        this.startTime = System.DateTime.UtcNow;
    }

    public void SetPlayerId(int _id)
    {
        playerId = _id;
    }

    public void SetHealthBarValue(float _health, float _maxHealth)
    {
        healthCounter.text = _health.ToString();
        healthSlider.SetValuePercent(_health, _maxHealth);
    }

    public void SetResourceBarValue(float _value, float _maxValue)
    {
        resourceSlider.SetValuePercent(_value, _maxValue);
    }

    public void SetTimerValue(TimeSpan _ts)
    {
        this.time.text = _ts.Minutes.ToString("00") + ":" + _ts.Seconds.ToString("00");
    }

    public void OnClickAbilityButtons(Button button)
    {
        if (isOnGlobalCooldown) return;
        //GameObject localPlayer = GameObject.FindGameObjectWithTag("LocalPlayer");
        //int id = localPlayer.GetComponent<PlayerManager>().id;
        int buttonNumber = int.Parse(button.name[button.name.Length - 1] + "");
        Vector3 mouseTarget = Vector3.zero;

        //localPlayer.GetComponent<PlayerAnimator>().PlayAttack2();

        Ray ray = this.GetComponent<Camera>().ScreenPointToRay(pos: Input.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            mouseTarget = hit.point;
        }

        ClientSend.TriggerAbility(playerId, buttonNumber, mouseTarget);
        StartCoroutine(SendCastCommandFixedTime(1f)); //"global cooldown ver implementacao"
    }

    public void PlayButtonCooldown(float _cooldown, int _hotbarIndex)
    {
        hotbar.transform.GetChild(_hotbarIndex).transform.GetChild(0).GetComponent<Cooldown>().Initialize(_cooldown);
    }

    private IEnumerator SendCastCommandFixedTime(float _delay)
    {
        isOnGlobalCooldown = true;
        yield return new WaitForSeconds(_delay);
        isOnGlobalCooldown = false;
    }

    public void AddEffect(int _id, float _lifeTime)
    {
        // ! instead of using id, pass on effect name and .Find in effects array (this array is specially for the HUD updater)
        // TODO: Add enumerated effects from server 
        if (effects.Length < _id) return;
        GameObject effect = effects[_id];
        CleanBuffs();

        GameObject effectExists = currentEffects.Find(e => e.GetComponent<EffectIcon1>().effectName == effect.GetComponent<EffectIcon1>().effectName);

        if (effectExists != null)
        {
            effectExists.GetComponent<EffectIcon1>().AddStack();
        }
        else
        {
            GameObject effectInstance = Instantiate(effect, effectsBar.transform);
            effectInstance.GetComponent<EffectIcon1>().Initialize(_lifeTime, this.gameObject);
            currentEffects.Add(effectInstance);
            RepositionEffects();
        }
    }

    public void RemoveEffect(int _id)
    {
        if (_id >= effects.Length) return;
        GameObject effect = effects[_id];
        //currentEffects.RemoveAll(e => e.GetComponent<EffectIcon>().effectName == effect.GetComponent<EffectIcon>().effectName);

        for (int i = 0; i < currentEffects.Count; i++)
        {
            if (currentEffects[i].GetComponent<EffectIcon1>().effectName == effect.GetComponent<EffectIcon1>().effectName)
            {
                currentEffects[i].GetComponent<EffectIcon1>().DestroyEffect();
                currentEffects.RemoveAt(i);
            }
        }
    }

    public void CleanBuffs()
    {
        currentEffects.RemoveAll(e => e == null);
        RepositionEffects();
    }

    private void RepositionEffects()
    {
        float position = 45f;
        float increments = 80f;

        foreach (GameObject e in currentEffects)
        {
            e.GetComponent<EffectIcon>().transform.position.Set(position, 0f, 0f);
            position += increments;
        }
    }
}