﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats
{
    public int strength {get; set;}
    public int intellect {get; set;}
    public int agility {get; set;}
    public int stamina {get; set;}
    public int armor {get; set;}
    public int magicResistance {get; set;}
    public int healthPer5 {get; set;}
    public int manaPer5 {get; set;}
    public float critChance {get; set;}
    public float attackSpeed {get; set;}
    public float physicalDamage {get; set;}    
    public float spellDamage {get; set;}
}
