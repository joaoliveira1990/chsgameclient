﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>Handle Slider GameObject</summary>
public class ResourceSlider : MonoBehaviour
{
    public Slider slider;
    public Image fill;
    private float maxValue = 100;

    public void SetColor(Color _color)
    {
        this.fill.color = _color;
    }

    public void SetValuePercent(float _value, float _maxValue)
    {
        if (_value > _maxValue) slider.value = this.maxValue;
        else slider.value = (int)(100 * _value) / _maxValue;
    }

    public void SetValuePercent(float _value)
    {
        if (_value > this.maxValue) slider.value = this.maxValue;
        else slider.value = (int)(100 * _value) / this.maxValue;
    }

    public void SetValue(float _value)
    {
        if (_value > this.maxValue) slider.value = this.maxValue;
        else slider.value = _value;
    }
}