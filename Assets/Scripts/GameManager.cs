﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public static Dictionary<int, PlayerManager> players = new Dictionary<int, PlayerManager>();
    public static Dictionary<int, ItemSpawner> itemSpawners = new Dictionary<int, ItemSpawner>();
    public static Dictionary<int, ProjectileManager> projectiles = new Dictionary<int, ProjectileManager>();
    public static Dictionary<int, EnemyManager> enemies = new Dictionary<int, EnemyManager>();

    public GameObject localPlayerPrefab;
    public int localPlayerId = 0;
    public GameObject playerPrefab;
    public GameObject itemSpawnerPrefab;
    public GameObject projectilePrefab;
    public GameObject[] projectilePrefabs;
    public GameObject[] aoePrefabs;
    public GameObject[] enemyPrefabs;
    public Camera playerCam;
    public Camera playerCamPrefab;
    public Camera playerCamInstance;

    public PlayerController localPlayer;

    //public int selectedChampion = 0;
    public Stats playerStats;
    public int selectedClass = 0;
    public int selectedSpec = 0;
    public int team = 1;

    // Event Variables

    public int time = 0;
    private Coroutine counter;
    private bool isRunning = true;
    public string mouseMode = "none"; // "none", "attack", "support"

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }
    }

    // public void SetCamera(Camera _playerCam)
    // {
    //     this.playerCam = _playerCam;
    // }
    public bool isCameraSet()
    {
        return this.playerCamInstance != null;
    }

    public void SpawnPlayer(int _id, string _username, int _selectedClass, int _selectedSpec, int _team, Vector3 _position, Quaternion _rotation, float _health, float _mana, int _gold)
    {
        GameObject player;
        _position.Set(_position.x, _position.y = 0.1f, _position.z);

        // - Create Local Player & Camera
        if (_id == Client.instance.myId)
        {
            player = Instantiate(localPlayerPrefab, _position, _rotation);
            player.GetComponent<PlayerManager>().Initialize(_id, _username, selectedClass, selectedSpec, _team, _health, _mana);
            players.Add(_id, player.GetComponent<PlayerManager>());

            this.playerCamInstance = Instantiate(playerCam);
            this.playerCamInstance.GetComponent<CameraManager>().Initialize(player);
            this.playerCamInstance.GetComponent<HudController>().SetGold(_gold);
            this.playerCamInstance.GetComponent<HudController>().UpdateStats(playerStats);
        }
        else
        {
            player = Instantiate(playerPrefab, _position, _rotation);
            player.GetComponent<PlayerManager>().Initialize(_id, _username, _selectedClass, _selectedSpec, _team, _health, _mana);
            players.Add(_id, player.GetComponent<PlayerManager>());
        }
    }

    public void CreateItemSpawner(int _spawnerId, Vector3 _position, bool _hasItem)
    {
        GameObject _spawner = Instantiate(itemSpawnerPrefab, _position, itemSpawnerPrefab.transform.rotation);
        _spawner.GetComponent<ItemSpawner>().Initialize(_spawnerId, _hasItem);
        itemSpawners.Add(_spawnerId, _spawner.GetComponent<ItemSpawner>());
    }

    public void SpawnProjectile(int _id, string _type, Vector3 _positionCaster, int _projectileId, Vector3 _positionTarget, float _range, int _team)
    {
        Vector3 _projectileSpawnPosition = new Vector3(_positionCaster.x, _positionCaster.y + 1f, _positionCaster.z);
        GameObject target = null;

        switch (_type)
        {
            case "player":
                target = players[_id].gameObject;
                // _projectile.GetComponent<RangedProjectile>().Initialize(players[_id].gameObject);
                break;
            case "enemy":
                target = enemies[_id].gameObject;
                // _projectile.GetComponent<RangedProjectile>().Initialize(enemies[_id].gameObject);
                break;
            case "ground":
                target = null;
                // _projectile.GetComponent<RangedProjectile>().Initialize(enemies[_id].gameObject);
                break;
            default:
                return;
        }

        GameObject _projectile = Instantiate(projectilePrefabs[_projectileId], _projectileSpawnPosition, Quaternion.identity);
        if (target)
        {
            _projectile.GetComponent<RangedProjectile>().Initialize(target);
        }
        else 
        {//skillshot
            _projectile.GetComponent<RangedProjectile>().Initialize(_positionTarget, _range, _team);
        }
    }

    public void SpawnAoe(Vector3 _spawnPosition, float _radius, float _destroyAfter, int _aoeModel, int _parentId, string _parentType)
    {
        Vector3 _aoeSpawnPosition = new Vector3(_spawnPosition.x, _spawnPosition.y + 0.1f, _spawnPosition.z);
        
        Debug.Log($"spawn aoe at -> {_aoeSpawnPosition}");
        
        //GameObject aoe = Instantiate(aoePrefabs[_aoeModel], _aoeSpawnPosition, Quaternion.identity);
        GameObject aoe = null;
        if(_parentId > 0)
        {
            if (_parentType == "enemy")
            {
                aoe = Instantiate(aoePrefabs[_aoeModel], enemies[_parentId].GetComponent<EnemyManager>().shootOrigin);
            }
            else if(_parentType == "player")
            {
                aoe = Instantiate(aoePrefabs[_aoeModel], players[_parentId].GetComponent<PlayerManager>().shootOrigin);
            }
        }
        else 
        {
            aoe = Instantiate(aoePrefabs[_aoeModel], _aoeSpawnPosition, Quaternion.identity);
        }

        //aoe component shitty name, should be circleAoe 
        if (_radius > 0 && aoe != null)
        {
            aoe.GetComponent<Aoe>().Initialize(_radius, _destroyAfter);        
        } 
        else
        {
            aoe.GetComponent<Cone>().Initialize(_destroyAfter);        
        } 
    }
    public void SpawnEnemy(int _id, int _enemyType, Vector3 _position, float _maxHealth)
    {
        GameObject enemy;

        enemy = Instantiate(enemyPrefabs[_enemyType], _position, Quaternion.identity);

        enemy.GetComponent<EnemyManager>().Initialize(_id, _maxHealth);
        //enemy.GetComponent<NavMeshAgent>().radius = 0.0001f;
        enemies.Add(_id, enemy.GetComponent<EnemyManager>());
    }

    // Event System
    // TODO?: Have some player manager stuff here?
}
