﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthSlider : MonoBehaviour
{
    public GameObject player;
    private Camera playerCam;
    public Slider slider;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // TODO: Maybe only need to fetch camInstance once (passed by ref)

        if (GameManager.instance.localPlayerId > 0)
        {
            PlayerManager localPlayer = GameManager.players[GameManager.instance.localPlayerId];
            if (GameManager.instance.isCameraSet())
            {
                playerCam = GameManager.instance.playerCamInstance;

                transform.LookAt(transform.position + GameManager.instance.playerCamInstance.transform.position);
            }
        }

        /*
        GameObject localPlayer = GameObject.FindGameObjectWithTag("LocalPlayer");
        if (localPlayer != null && localPlayer.GetComponent<CameraController>())
        {
            if (localPlayer.GetComponent<CameraController>().playerCamInstance != null)
            {
                playerCam = localPlayer.GetComponent<CameraController>().playerCamInstance;

                transform.LookAt(transform.position + playerCam.transform.forward );
            }
        }
        */
    }

    public void SetValue(float _health, float _maxHealth)
    {
        slider.value = (int)(100 * _health) / _maxHealth;
    }
}
