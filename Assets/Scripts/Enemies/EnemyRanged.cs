﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRanged : MonoBehaviour
{
    public int id;
    public float health;
    public float maxHealth = 100f;
    public EnemyState state;
    public GameObject floatingCombatText;
    private float lastHitDamage = 0f;

    public void Initialize(int _id)
    {
        id = _id;
        health = maxHealth;
        state = EnemyState.idle;
    }

    public void SetHealth(float _health)
    {

        //floating combat text
        lastHitDamage = health - _health;
        if (floatingCombatText)
        {
            ShowFloatingCombatText(lastHitDamage);
        }
        //floating combat text

        health = _health;

        for (int i = 0; i < this.transform.childCount; i++)
        {
            if (this.transform.GetChild(i).name == "HP Plate")
            {
                this.transform.GetChild(i).GetComponent<EnemySliders>().SetValue(_health, maxHealth);
            }
        }

        if (health <= 0f)
        {
            StartCoroutine(DestroyAfter(0.5f));
        }
    }

    private IEnumerator DestroyAfter(float _seconds)
    {
        GameManager.enemies.Remove(id);
        Destroy(gameObject);
        yield return new WaitForSeconds(_seconds);
    }

    private void ShowFloatingCombatText(float _damage)
    {
        GameObject floatingCombatTextInstance = Instantiate(floatingCombatText, transform.position + new Vector3(0f, 4f, 0f), Quaternion.identity, transform);
        floatingCombatTextInstance.GetComponent<TextMesh>().text = _damage.ToString();
    }

    public void SetState(int _state)
    {
        state = (EnemyState)_state;
        this.GetComponent<EnemyAnimator>().state = state;
    }
}