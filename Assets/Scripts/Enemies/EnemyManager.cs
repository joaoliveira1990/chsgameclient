﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;

public class EnemyManager : MonoBehaviour
{
    public int id;
    public float health;
    public float maxHealth;
    public EnemyState state;
    public GameObject floatingCombatText;
    private float lastHitDamage = 0f;
    public GameObject[] vfxFabs;

    public NavMeshAgent agent = null;
    public NavMeshPath path = null;

    private Vector3[] waypoints = null;

    public bool isDead = false;

    public Transform shootOrigin;

    private List<EffectVfx> currentEffects = new List<EffectVfx>();

    public void Initialize(int _id, float _maxHealth)
    {
        id = _id;
        maxHealth = _maxHealth;
        health = maxHealth;
        state = EnemyState.idle;

        agent = this.GetComponent<NavMeshAgent>();

        path = new NavMeshPath();
    }

    public void OnEnemyCast(float _castTime, string _castAnimation)
    {
        Transform castbar = this.gameObject.transform.Find("Castbar");
        if (castbar != null)
        {
            castbar.GetComponent<EnemySliders>().StartCastbar(_castTime);
            this.GetComponent<EnemyAnimator>().PlayCasting(_castTime, _castAnimation);
        }
    }

    public void StopCastbar()
    {
        this.gameObject.transform.Find("Castbar").GetComponent<EnemySliders>().StopCastbar();
    }

    public void SetHealth(float _health)
    {

        // Floating combat text
        lastHitDamage = health - _health;
        if (floatingCombatText)
        {
            ShowFloatingCombatText(lastHitDamage);
        }

        this.health = _health;

        for (int i = 0; i < this.transform.childCount; i++)
        {
            if (this.transform.GetChild(i).name == "HP Plate")
            {
                this.transform.GetChild(i).GetComponent<EnemySliders>().SetValue(_health, maxHealth);
            }
        }

        if (health <= 0f)
        {
            isDead = true;
            StartCoroutine(DestroyAfter(5f));
        }

    }

    private IEnumerator DestroyAfter(float _seconds)
    {
        //play animation
        this.GetComponent<EnemyAnimator>().PlayDead();
        yield return new WaitForSeconds(_seconds);
        GameManager.enemies.Remove(id);
        this.transform.GetChild(0).GetComponent<ColliderOnOver>().ForceExit();
        Destroy(gameObject);
    }

    public void UpdateEnemyPath(Vector3[] _waypoints, bool _isStopped)
    {
        if (_isStopped)
        {
            StopPath();
            return;
        }

        agent.isStopped = false;

        this.waypoints = _waypoints;
        Quaternion rotationZeroX = Quaternion.LookRotation(waypoints[1] - this.transform.position);
        rotationZeroX.x = 0;
        this.transform.rotation = rotationZeroX;
        agent.SetDestination(waypoints[waypoints.Length - 1]);

        if (waypoints.Length > agent.path.corners.Length)
        {
            for (int i = 0; i < agent.path.corners.Length; i++)
            {
                Vector3 waypoint = waypoints[i];
                agent.path.corners[i].Set(waypoint.x, waypoint.y, waypoint.z);
            }
        }
        else
        {
            for (int i = 0; i < waypoints.Length; i++)
            {
                Vector3 waypoint = waypoints[i];
                agent.path.corners[i].Set(waypoint.x, waypoint.y, waypoint.z);
            }
        }

    }

    private void ShowFloatingCombatText(float _damage)
    {
        GameObject floatingCombatTextInstance = Instantiate(floatingCombatText, transform.position, Quaternion.identity, transform);
        //floatingCombatTextInstance.GetComponent<TextMesh>().text = _damage.ToString();
        floatingCombatTextInstance.transform.GetChild(0).GetComponentInChildren<TMP_Text>().text = Mathf.Abs(_damage).ToString();
    }

    public void StopPath()
    {
        agent.isStopped = true;
        //path = null;
        waypoints = Array.Empty<Vector3>();
    }
    public void SetAgentSpeed(float _speed)
    {
        agent.speed = _speed;
    }
    public void SetState(int _state)
    {
        state = (EnemyState)_state;
        this.GetComponent<EnemyAnimator>().state = state;
    }

    public void AddEffect(EffectVfx effectVfxInstance)
    {
        CleanEffects();
        this.currentEffects.Add(effectVfxInstance);
    }

    public void CleanEffects()
    {
        this.currentEffects.RemoveAll(e => e == null);
    }

    public EffectVfx GetEffectByName(AllEffects _effectName)
    {
        return this.currentEffects.Find(effect => effect.id == _effectName);
    }

    public int GetNumberOfStacksFromEffect(AllEffects _effectName)
    {
        EffectVfx foundEffect = this.currentEffects.Find(effect => effect.id == _effectName);

        if (foundEffect == null) return 0;
        else return foundEffect.stackCount;
    }
}

public enum EnemyState
{
    idle,
    patrol,
    chase,
    attack
}
