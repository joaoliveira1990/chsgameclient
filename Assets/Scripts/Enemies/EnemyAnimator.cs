﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAnimator : MonoBehaviour
{
    NavMeshAgent agent;
    public Animator anim;
    public EnemyState state = EnemyState.idle;
    public GameObject[] vfx;

    float motionSmoothTime = .1f;
    void Start()
    {
        agent = gameObject.GetComponent<NavMeshAgent>();
    }
    void Update()
    {

        float speed = agent.velocity.magnitude / agent.speed;

        switch (state)
        {
            case EnemyState.idle:
                anim.SetBool("IsEnemyMoving", false);
                anim.SetFloat("Speed", speed, motionSmoothTime, Time.deltaTime);
                break;
            case EnemyState.chase:
                anim.SetBool("IsEnemyMoving", true);
                anim.SetFloat("Speed", speed, motionSmoothTime, Time.deltaTime);
                break;
            case EnemyState.attack:
                anim.SetBool("IsEnemyMoving", true);
                anim.SetFloat("Speed", speed, motionSmoothTime, Time.deltaTime);
                break;
            default:
                anim.SetBool("IsEnemyMoving", true);
                break;
        }
    }

    public void SetAnimationSpeed(float _attackSpeed)
    {
        anim.SetBool("IsEnemyMoving", false);
        anim.SetTrigger("IsEnemyAttacking"); //será?
        anim.SetFloat("AttackSpeed", 1f / _attackSpeed);
    }

    public void PlayCasting(float _castTime, string _castAnimation)
    {
        anim.SetTrigger("IsEnemyCasting");
        anim.SetBool("IsEnemyMoving", false);
        anim.Play(_castAnimation);
        StartCoroutine(FinishCasting(_castTime));
    }

    private IEnumerator FinishCasting(float _castTime)
    {
        yield return new WaitForSeconds(_castTime);
        anim.SetTrigger("IsEnemyAttacking");
    }

    public void PlayDead()
    {
        anim.SetTrigger("EnemyDied");
    }
}
