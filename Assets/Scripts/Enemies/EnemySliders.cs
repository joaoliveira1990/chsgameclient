﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySliders : MonoBehaviour
{
    private Camera playerCam;
    public Slider slider;


    private float value;
    private float maxValue;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // TODO: Maybe only need to fetch camInstance once (passed by ref)

        if (GameManager.instance.localPlayerId > 0)
        {
            PlayerManager localPlayer = GameManager.players[GameManager.instance.localPlayerId];
            if (GameManager.instance.isCameraSet())
            {
                playerCam = GameManager.instance.playerCamInstance;

                transform.LookAt(transform.position + playerCam.transform.forward);
            }
        }
    }


    public void StartCastbar(float _castTime)
    {
        this.gameObject.SetActive(true);
        value = 0f;
        maxValue = _castTime;
        slider.value = value;
        slider.maxValue = maxValue;
        StartCoroutine(UpdateCastBar(_castTime));
    }

    public void StopCastbar()
    {
        StopCoroutine("UpdateCastBar");
        this.gameObject.SetActive(false);
    }

    private IEnumerator UpdateCastBar(float _castTime)
    {
        slider.value = value;
        yield return new WaitForSeconds(_castTime / 10);
        value += _castTime / 10;
        if (value < _castTime)
        {
            StartCoroutine(UpdateCastBar(_castTime));
        }
        else
        {
            StopCastbar();
        }

    }

    public void SetValue(float _health, float _maxHealth)
    {
        slider.value = (int)(100 * _health) / _maxHealth;
    }
}
