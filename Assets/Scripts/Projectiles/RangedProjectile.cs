﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Attach to projectile to make guided missile that self destroys on collision with target
 * Analogous script in server. Projectiles run in parallel in client - server
 */
public class RangedProjectile : MonoBehaviour
{
    /* Target to be followed until collision w/ projectile */
    private GameObject target = null;
    private Vector3 targetSkillshot = Vector3.zero;
    private bool isSkillshot = false;
    private int casterTeam = 0;
    private Vector3 initialPosition = Vector3.zero;
    private float skillshotRange = 0f;
    /* Update projectile once target is set */
    private bool isTargetSet = false;

    [SerializeField] private float chaseSpeed = 6.0f;
    [SerializeField] private float hitRange = 2.0f;
    [SerializeField] private GameObject vfxOnHit;

    public void Initialize(GameObject _target)
    {
        this.target = _target;
        this.isTargetSet = true;
    }
    public void Initialize(Vector3 _target, float _range, int _team)
    {
        this.targetSkillshot = _target;
        this.isTargetSet = true;
        this.isSkillshot = true;
        this.skillshotRange = _range;
        this.casterTeam = _team;
        this.initialPosition = transform.position;
    }

    private void FixedUpdate()
    {
        if (this.isSkillshot)
        {
            Vector3 targetToPos = targetSkillshot - transform.position;
            Vector3 targetToPosHoriz = new Vector3(targetToPos.x, 0f, targetToPos.z);

            GuidedMovementUpdate(targetToPosHoriz);
            CheckSkillshotCollision();
        }
        else
        {
            // Destroy if target dies
            if (this.target == null && this.isTargetSet) Destroy(this.gameObject);
            if (this.target == null) return;

            Vector3 targetToPos = target.transform.position - transform.position;
            Vector3 targetToPosHoriz = new Vector3(targetToPos.x, 0f, targetToPos.z);

            GuidedMovementUpdate(targetToPosHoriz);
            CheckColision(targetToPosHoriz);
        }

    }

    /* Movement update based on horizontal projection of vector to target */
    private void GuidedMovementUpdate(Vector3 targetToPosHoriz)
    {
        transform.forward = targetToPosHoriz;
        Vector3 _movement = transform.forward * chaseSpeed;

        this.GetComponent<CharacterController>().Move(_movement);

        Vector3 lookMovement = _movement;
        transform.rotation = Quaternion.LookRotation(lookMovement);
    }

    /* Check collision using horizontal projection of vector to target */
    private void CheckColision(Vector3 targetToPosHoriz)
    {
        if (targetToPosHoriz.magnitude <= hitRange)
        {
            Vector3 targetTransform = Vector3.zero;
            if (target.TryGetComponent(out EnemyAnimator e)) targetTransform = e.transform.position;
            else if (target.TryGetComponent(out PlayerAnimator p)) targetTransform = p.transform.position;

            if (this.vfxOnHit != null && targetTransform != null) Instantiate(this.vfxOnHit, targetTransform, this.vfxOnHit.transform.rotation);
            Destroy(this.gameObject);
        }
    }

    private void CheckSkillshotCollision()
    {
        if ((this.initialPosition - transform.position).magnitude > this.skillshotRange)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        Debug.Log("HIT - t");
        Debug.Log(collision.gameObject.tag);
        //if(collision.gameObject.tag == "LocalPlayer") return;
        if (collision.gameObject.transform.parent.TryGetComponent(out PlayerManager p)) 
        {
            if (p.team == this.casterTeam)
            {
                return;
            }
            else
            {
                if (this.vfxOnHit != null) Instantiate(this.vfxOnHit, this.transform.position, this.vfxOnHit.transform.rotation);
                Destroy(this.gameObject);
            }
        }
        else if (collision.gameObject.transform.parent.TryGetComponent(out EnemyManager e)) 
        {
            if (this.vfxOnHit != null) Instantiate(this.vfxOnHit, this.transform.position, this.vfxOnHit.transform.rotation);
            Destroy(this.gameObject);
        }
    }
}
