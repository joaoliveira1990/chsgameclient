﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform playerTransform;
    public Camera playerCam;
    public Camera playerCamInstance;

    [SerializeField] private float cameraVelocity = 20.0f;
    private bool cameraSnapCenter = true;
    private float distanceTraveled = 0f;
    private int zoomMin = 5; // zoom is 'y' value
    private int zoomMax = 20;

    public float sensitivity = 100f;
    public float clampAngle = 85f;

    private void Start()
    {
        playerCamInstance = Instantiate(playerCam);

        Vector3 zoomedPosition = playerTransform.position;
        zoomedPosition.y = zoomMax;
        playerCamInstance.transform.position = zoomedPosition;
        CenterCameraOnPlayer();
    }

    private void Update()
    {
        distanceTraveled = Time.deltaTime * cameraVelocity;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            cameraSnapCenter = true;
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {   
            cameraSnapCenter = false;
        }

        if (cameraSnapCenter)
        {
            CenterCameraOnPlayer();
        }

        ZoomCamera();

        if (Input.GetKeyDown(KeyCode.Y))
        {
            cameraSnapCenter = !cameraSnapCenter;
        }

        if (cameraSnapCenter == false)
        //if (Cursor.lockState == CursorLockMode.Locked && cameraSnapCenter == false)
        {
            //Look();
            CheckMousePositionOnScreen();
        }

        //Debug.DrawRay(transform.position, transform.forward * 2, Color.red);

    }

    private void ZoomCamera()
    {
        int mouseWheel = (int)Input.mouseScrollDelta.y;

        if (mouseWheel != 0)
        {
            if (mouseWheel == 1 && playerCamInstance.transform.position.y > zoomMin)
            {
                playerCamInstance.transform.Translate(Vector3.forward * distanceTraveled * 5);
            }
            else if (mouseWheel == -1 && playerCamInstance.transform.position.y < zoomMax)
            {
                playerCamInstance.transform.Translate(Vector3.back * distanceTraveled * 5);
            }
        }
    }

    private void CenterCameraOnPlayer()
    {
            float angleX = playerCamInstance.transform.localEulerAngles.x * Mathf.Deg2Rad;
            float angleY = playerCamInstance.transform.localEulerAngles.y * Mathf.Deg2Rad;
            float height = playerCamInstance.transform.position.y;

            float offX = height * Mathf.Sin(angleY) / Mathf.Tan(angleX);
            float offZ = height * Mathf.Cos(angleY) / Mathf.Tan(angleX);

            Vector3 off = new Vector3(-offX, height, -offZ);

            Vector3 snappedPos = playerTransform.position + off;
            snappedPos.y = height;
            playerCamInstance.transform.position = snappedPos;
    }

    private void CheckMousePositionOnScreen()
    {
        Vector2 mousePosition = Input.mousePosition;

        if (mousePosition.x < 20)
        {
            playerCamInstance.transform.Translate(Vector3.left * distanceTraveled);
        }
        else if (mousePosition.x > Screen.width - 20)
        {
            playerCamInstance.transform.Translate(Vector3.right * distanceTraveled);
        }

        if (mousePosition.y < 20)
        {
            float angleY = playerCamInstance.transform.localEulerAngles.y * Mathf.Deg2Rad;
            Vector3 trans = Vector3.back * Mathf.Cos(angleY) + Vector3.left * Mathf.Sin(angleY);
            playerCamInstance.transform.Translate(trans * distanceTraveled, Space.World);
        }
        else if (mousePosition.y > Screen.height - 20)
        {
            float angleY = playerCamInstance.transform.localEulerAngles.y * Mathf.Deg2Rad;
            Vector3 trans = Vector3.forward * Mathf.Cos(angleY) + Vector3.right * Mathf.Sin(angleY);
            playerCamInstance.transform.Translate(trans * distanceTraveled, Space.World);
        }
    }


    private void Look()
    {
        //float _mouseVertical = -Input.GetAxis("Mouse Y");
        //float _mouseHorizontal = Input.GetAxis("Mouse X");

        //verticalRotation += _mouseVertical * sensitivity * Time.deltaTime;
        //horizontalRotation += _mouseHorizontal * sensitivity * Time.deltaTime;

        //verticalRotation = Mathf.Clamp(verticalRotation, -clampAngle, clampAngle);

        //transform.localRotation = Quaternion.Euler(verticalRotation, 0f, 0f);
        //playerTransform.rotation = Quaternion.Euler(0f, horizontalRotation, 0f);
    }

    private void ToggleCursorMode()
    {
        Cursor.visible = !Cursor.visible;

        if (Cursor.lockState == CursorLockMode.None)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }

}
