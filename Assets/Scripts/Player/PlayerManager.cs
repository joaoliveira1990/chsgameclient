﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using TMPro;

public class PlayerManager : MonoBehaviour
{
    public int id;
    public string username;
    public float health;
    public float maxHealth;
    public float maxMana = 0f;
    public float mana = 0f;
    public int selectedClass;
    public int selectedSpec;
    public int team;
    public int itemCount = 0;
    private float lastHitDamage = 0f;
    public MeshRenderer model;
    public PlayerState state;
    public GameObject floatingCombatText;
    public GameObject floatingCombatHealedText;
    public GameObject[] playerModels = null;
    public GameObject playerModel = null;
    private List<EffectVfx> currentEffects = new List<EffectVfx>();

    public NavMeshAgent agent = null;
    public NavMeshPath path = null;

    public int targetId = -1;
    public string targetTag = "";
    public GameObject target = null;
    public Text namePlate = null;
    [NonSerialized] private List<GameObject> glowVfx = new List<GameObject>();
    [SerializeField] public Transform shootOrigin = null;

    private Vector3[] waypoints = null;
    private Vector3 lastKnownPosition = Vector3.zero;
    private bool hasLastKnownPosition = false;
    // ! Add effects of player here (track Vfx)
    // [NonSerialized] public EffectVfx[] vfxEffects;

    private bool isDead = false;

    private PlayerController playerControllerComponent;

    // TODO: Erase
    public void Initialize(int _id, string _username)
    {
        id = _id;
        username = _username;
        health = this.maxHealth;

        agent = this.GetComponent<NavMeshAgent>();
        path = new NavMeshPath();
        playerControllerComponent = this.gameObject.GetComponent<PlayerController>();
    }

    public void Initialize(int _id, string _username, int _selectedClass, int _selectedSpec, int _team, float _health, float _mana)
    {
        this.id = _id;
        this.username = _username;
        this.maxHealth = _health;
        this.maxMana = _mana;
        this.mana = maxMana;
        this.health = _health;
        this.selectedClass = _selectedClass;
        this.selectedSpec = _selectedSpec;
        this.team = _team;
        this.namePlate.text = _username;

        this.agent = this.GetComponent<NavMeshAgent>();
        this.path = new NavMeshPath();
        this.playerControllerComponent = this.gameObject.GetComponent<PlayerController>();

        int playerModelId = _selectedClass - 1;

        GameObject model = Instantiate(playerModels[playerModelId], transform) as GameObject;
        model.transform.parent = gameObject.transform.GetChild(0).transform;
        playerModel = model;
        GetComponent<PlayerAnimator>().SetPlayerModel(playerModel.GetComponent<Animator>());

        if(this.gameObject.transform.GetChild(0).GetChild(0).TryGetComponent<CastGlow>(out CastGlow castGlow)) {
          this.glowVfx = castGlow.glowVfx;
        }

        ShowResourceBar();
    }

    private void Update()
    {
        if (waypoints != null && target == null && targetId > 0)
        {
            if (waypoints.Length > 0)
            {
                lastKnownPosition = waypoints[waypoints.Length - 1];
                hasLastKnownPosition = true;
            }
            else if (hasLastKnownPosition)
            {
                if (Vector3.Distance(transform.position, lastKnownPosition) > 0.2f)
                {
                    agent.Warp(lastKnownPosition);
                }
                hasLastKnownPosition = false;
            }
        }
        else
        {
            // Agent stopped
        }
    }

    public void AddEffect(EffectVfx effectVfxInstance)
    {
        CleanEffects();
        this.currentEffects.Add(effectVfxInstance);
    }

    public void CleanEffects()
    {
        this.currentEffects.RemoveAll(e => e == null);
    }

    public EffectVfx GetEffectByName(AllEffects _effectName)
    {
        return this.currentEffects.Find(effect => effect.id == _effectName);
    }

    public int GetNumberOfStacksFromEffect(AllEffects _effectName)
    {
        EffectVfx foundEffect = this.currentEffects.Find(effect => effect.id == _effectName);

        if (foundEffect == null) return 0;
        else return foundEffect.stackCount;
    }

    //not for the local player
    public void SetSelectedChampion(int selectedChampion)
    {
        GameObject model = Instantiate(playerModels[selectedChampion], transform) as GameObject;
        model.transform.parent = gameObject.transform.GetChild(0).transform;
        playerModel = model;
        GetComponent<PlayerAnimator>().SetPlayerModel(playerModel.GetComponent<Animator>());
    }
    public void ShowResourceBar()
    {
        if (mana > 0)
        {
            this.gameObject.transform.Find("ManaBar").gameObject.SetActive(true);
        }
        else
        {
            this.gameObject.transform.Find("RagePlate").gameObject.SetActive(true);
        }
    }

    public void OnPlayerCasted(float _castTime, string _anim)
    {
        this.gameObject.transform.Find("Castbar").GetComponent<PlayerSliders>().StartCastbar(_castTime);
        this.GetComponent<PlayerAnimator>().PlayCastAnimation(_anim, _castTime);

        UpdateCastGlow(true);
        StartCoroutine(UpdateCastGlowTimed(false, _castTime));

    }

    public void StopCastbar()
    {
        this.gameObject.transform.Find("Castbar").GetComponent<PlayerSliders>().StopCastbar();
        UpdateCastGlow(false);
    }

    private void UpdateCastGlow(bool isActive) {
        for(int i = 0; i < this.glowVfx.Count; i++)
        {
          this.glowVfx[i].SetActive(isActive);
        }
    }

    public void SetRage(float _rage)
    {
        // TODO: Avoid using Find. Use a GameManager reference instead
        this.gameObject.transform.Find("RagePlate").GetComponent<PlayerSliders>().SetRageValue(_rage);
        GameManager.instance.playerCamInstance.GetComponent<HUDUpdater>().SetResourceBarValue(_rage, 100);
    }

    public void SetMana(float _mana, float _maxMana)
    {
        // TODO: Avoid using Find. Use a GameManager reference instead
        this.gameObject.transform.Find("ManaBar").GetComponent<PlayerSliders>().SetManaValue(_mana, _maxMana);
        GameManager.instance.playerCamInstance.GetComponent<HUDUpdater>().SetResourceBarValue(_mana, _maxMana);
    }

    public void SetHealth(float _health)
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            if (this.transform.GetChild(i).name == "HP Plate")
            {
                this.transform.GetChild(i).GetComponent<PlayerSliders>().SetHealthValue(_health, this.maxHealth);
            }
        }

        //floating combat text
        lastHitDamage = health - _health;
        ShowFloatingCombatText(lastHitDamage);

        //floating combat text
        health = _health;

        if (health <= 0f)
        {
            Die();
        }
    }
    public void SetXpLevel(int _level, int _currentXp, bool _isLevelUp)
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            if (this.transform.GetChild(i).name == "HP Plate")
            {
                this.transform.GetChild(i).GetChild(0).GetComponent<Text>().text = _level +"";
            }
        }
        if (_isLevelUp)
        {
            this.GetComponent<PlayerAnimator>().PlayLevelUpVfx();
        }
    }
    public void SetAgentSpeed(float _speed)
    {
        agent.speed = _speed;
    }
    public void SetPlayerSize(float _sizeMultiplier, float _duration)
    {
        Vector3 scaleChange = new Vector3(_sizeMultiplier, _sizeMultiplier, _sizeMultiplier);
        Vector3 initialSize = playerModel.transform.localScale;

        playerModel.transform.localScale += scaleChange;

        StartCoroutine(IncreasePlayerSizeFor(_duration, initialSize));
    }
    private IEnumerator IncreasePlayerSizeFor(float _duration, Vector3 _initialSize)
    {
        yield return new WaitForSeconds(_duration);
        playerModel.transform.localScale = _initialSize;
    }
    private void ShowFloatingCombatText(float _damage)
    {
        GameObject floatingText = null;
        if (_damage < 0)
        {
            floatingText = floatingCombatHealedText;
        }
        else
        {
            floatingText = floatingCombatText;
        }

        GameObject floatingCombatTextInstance = Instantiate(floatingText, transform.position, Quaternion.identity, transform);
        //floatingCombatTextInstance.GetComponent<TextMesh>().text = Mathf.Abs(_damage).ToString();
        floatingCombatTextInstance.transform.GetChild(0).GetComponentInChildren<TMP_Text>().text = Mathf.Abs(_damage).ToString();
    }

    public void SetState(int _state, int _targetId, string _targetTag)
    {
        state = (PlayerState)_state;
        targetId = _targetId;
        targetTag = _targetTag;

        if ((state == PlayerState.attack || state == PlayerState.chase) && _targetId > 0)
        {
            hasLastKnownPosition = false;
            switch (targetTag)
            {
                case "Enemy":
                    target = GameManager.enemies[targetId].gameObject;
                    break;
                case "Player":
                    target = GameManager.players[targetId].gameObject;
                    break;
            }
        }
        else
        {
            target = null;
        }

        this.GetComponent<PlayerAnimator>().state = state;
    }

    public void Die()
    {
        //playerModel.GetComponent<MeshRenderer>().enabled = true;
        this.gameObject.transform.GetChild(0).GetChild(0).GetComponent<Animator>().enabled = false;
        StopAgent();
        //this.gameObject.SetActive(false);
        isDead = true;
    }

    public void Respawn(Vector3 _spawnPosition)
    {
        //playerModel.GetComponent<MeshRenderer>().enabled = true;
        this.gameObject.transform.GetChild(0).GetChild(0).GetComponent<Animator>().enabled = true;
        //this.gameObject.SetActive(true);
        agent.Warp(_spawnPosition);
        SetHealth(this.maxHealth);
        isDead = false;
    }

    public void UpdatePlayerPath(Vector3[] _waypoints)
    {
        agent.updatePosition = true;

        StopCastbar();
        UpdateAgent(_waypoints);
    }

    public void PositionServerUpdate(Vector3 _position)
    {
        if (Vector3.Distance(_position, transform.position) > 0.5f)
        {
            transform.position = Vector3.Lerp(transform.position, _position, 0.5f);
            // Re calculate path to avoid jittering. Needs better verification.
            NavMesh.CalculatePath(transform.position, waypoints[waypoints.Length - 1], NavMesh.AllAreas, path);
            agent.SetPath(path);
        }
    }

    public void PositionWarp(Vector3 _position)
    {
        agent.isStopped = true;
        _position.y = 0.06f;
        agent.Warp(_position);
    }

    public void RotationServerUpdate(Quaternion _rotation)
    {
        // TODO: Have more advanced rotation reconciliation
        _rotation.x = 0;
        transform.rotation = _rotation;
    }

    public void StopAgent()
    {
        GetComponent<PlayerAnimator>().PlayIdle();
        agent.isStopped = true;
        // path = null;
        waypoints = Array.Empty<Vector3>();
    }

    private void UpdateAgent(Vector3[] _waypoints)
    {

        // GetComponent<PlayerAnimator>().PlayMoving();

        _waypoints[_waypoints.Length - 1].y = 0.06f;

        this.waypoints = _waypoints;

        agent.isStopped = false;

        if (_waypoints.Length > 1)
        {
            Quaternion rotationZeroX = Quaternion.LookRotation(_waypoints[1] - this.transform.position);
            rotationZeroX.x = 0;
            this.transform.rotation = rotationZeroX;
        }
        agent.SetDestination(_waypoints[_waypoints.Length - 1]);
    }

    private IEnumerator UpdateCastGlowTimed(bool isActive, float _castTime)
    {
        yield return new WaitForSeconds(_castTime);
        UpdateCastGlow(isActive);
    }
}

