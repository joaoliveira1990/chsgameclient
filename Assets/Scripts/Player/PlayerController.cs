﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using System;

public class PlayerController : MonoBehaviour
{
    public Transform playerTransform;
    public GameObject clickVfx;

    [SerializeField] private float rotateSpeedMovement = 0.1f;
    [SerializeField] private float rotateVelocity;

    bool canInitializeHotbar = true;

    private void Start()
    {
        GameManager.instance.localPlayer = this;
    }

    private void Update()
    {
        // - Key presses
        if (Input.GetKeyDown(KeyCode.Mouse0)) // Left Mouse
        {
            // TODO: get player position -> mouse coords vector
        }

        if (Input.GetKeyDown(KeyCode.Mouse1)) // Right Mouse
        {
            PlayerMainAction();
        }
    }


    private void PlayerMainAction()
    {
        Vector3 playerPosition = Vector3.zero;
        Vector3 newPosition = Vector3.zero;

        Ray ray = GameManager.instance.playerCamInstance.ScreenPointToRay(pos: Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            newPosition = hit.point;
            DrawPrimitives.instance.DrawVerticalRay(newPosition, 20.0f);

            ClientSend.PlayerAction(newPosition);

            Vector3 effectposition = newPosition; // !
            effectposition.y += 0.05f;
            Instantiate(this.clickVfx, effectposition, Quaternion.Euler(0f, 0f, 0f));
        }


        //get player position -> mouse coords vector
        //ClientSend.PlayerShoot(camTransform.forward
    }
}
