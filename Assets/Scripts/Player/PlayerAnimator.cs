﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class PlayerAnimator : MonoBehaviour
{
    NavMeshAgent agent;
    public Animator anim;
    public PlayerState state = PlayerState.idle;
    public GameObject[] vfx;

    float motionSmoothTime = 0.1f;

    private Hashtable triggerTable = new Hashtable();

    void Start()
    {
        agent = gameObject.GetComponent<NavMeshAgent>();

        triggerTable.Add("attack_01", "IsPlayerAttacking");
        triggerTable.Add("attack_02", "IsPlayerAttacking");
        triggerTable.Add("cast_01", "IsPlayerCasting");
    }

    public void SetPlayerModel(Animator animator)
    {
        anim = animator;
    }

    void Update()
    {
        if (anim)
        {
            float speed = agent.velocity.magnitude / agent.speed;
            switch (state)
            {
                case PlayerState.idle:
                    anim.SetBool("IsPlayerMoving", false);
                    anim.SetFloat("Speed", speed, motionSmoothTime, Time.deltaTime);
                    break;
                case PlayerState.chase:
                    anim.SetBool("IsPlayerMoving", true);
                    anim.SetFloat("Speed", speed, motionSmoothTime, Time.deltaTime);
                    break;
                case PlayerState.attack:
                    anim.SetBool("IsPlayerMoving", true);
                    anim.SetFloat("Speed", speed, motionSmoothTime, Time.deltaTime);
                    break;
                default:
                    anim.SetBool("IsPlayerMoving", true);
                    anim.SetFloat("Speed", speed, motionSmoothTime, Time.deltaTime);
                    break;
            }
        }
    }

    // TODO: Change name to something clearer
    public void SetAnimationSpeed(float _attackSpeed)
    {
        anim.SetTrigger("IsPlayerAttacking");
        anim.SetBool("IsPlayerMoving", false);
        anim.SetFloat("AttackSpeed", 1f / _attackSpeed);
        //anim.Play("attack_01");
    }

    public void PlayAttack2(float _attackSpeed)
    {
        anim.SetTrigger("IsPlayerCasting");
        anim.SetBool("IsPlayerMoving", false);
        anim.SetFloat("AttackSpeed", 1f / _attackSpeed);
        //anim.Play("attack_01");
    }

    public void PlayPlayerAnimations(float _attackSpeed, bool _isCast)
    {
        if (_isCast) PlayAttack2(_attackSpeed);
        else SetAnimationSpeed(_attackSpeed);
    }

    public void PlayIdle()
    {
        /*
        anim.SetBool("IsPlayerMoving", false);
        anim.Play("idle_01");
        */
    }

    public void PlayMoving()
    {
        anim.SetBool("IsPlayerMoving", true);
        // anim.Play("idle_01");
    }

    public void PlayCasting()
    {
        anim.SetTrigger("IsPlayerCasting");
        anim.SetBool("IsPlayerMoving", false);
        anim.Play("cast_01");
    }

    public void PlayAnimation(string _animString)
    {
        if (_animString != "")
        {
            anim.SetTrigger((string)triggerTable[_animString]);
            anim.Play(_animString);
        }
    }

    public void PlayCastAnimation(string _animString, float _castTime)
    {
        if (_animString != "")
        {
            anim.SetBool("IsPlayerMoving", false);
            anim.SetTrigger((string)triggerTable[_animString]);
            anim.SetFloat("CastSpeed", 1f / (_castTime - 0.2f));
            anim.Play(_animString);
        }
    }

    public void PlayLevelUpVfx()
    {
        Destroy(Instantiate(this.vfx[0], transform.position, Quaternion.identity), 3f);
    }
}
