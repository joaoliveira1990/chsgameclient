﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>Generic Slider</summary>
public class PlayerSliders : MonoBehaviour
{
    public GameObject player;
    private Camera playerCam;
    public Slider slider;

    private float value;
    private float maxValue;

    void Start()
    {

    }

    void Update()
    {
        if (GameManager.instance.localPlayerId > 0)
        {
            // TODO: We should get localPlayer once
            PlayerManager localPlayer = GameManager.players[GameManager.instance.localPlayerId];
            if (GameManager.instance.isCameraSet())
            {
                playerCam = GameManager.instance.playerCamInstance;

                // TODO: Look at camera, not forward
                transform.LookAt(transform.position + playerCam.transform.forward);
            }
        }
    }

    public void StartCastbar(float _castTime)
    {
        this.gameObject.SetActive(true);
        value = 0f;
        maxValue = _castTime;
        slider.value = value;
        slider.maxValue = maxValue;
        StartCoroutine(UpdateCastBar(_castTime));
    }

    public void StopCastbar()
    {
        StopCoroutine("UpdateCastBar");
        this.gameObject.SetActive(false);
    }

    private IEnumerator UpdateCastBar(float _castTime)
    {
        slider.value = value;
        yield return new WaitForSeconds(_castTime / 10);
        value += _castTime / 10;
        if (value < _castTime)
        {
            StartCoroutine(UpdateCastBar(_castTime));
        }
        else
        {
            StopCastbar();
        }

    }

    public void SetHealthValue(float _health, float _maxHealth)
    {
        slider.value = (int)(100 * _health) / _maxHealth;
    }

    public void SetManaValue(float _mana, float _maxMana)
    {
        if (_mana > _maxMana) slider.value = 100;
        slider.value = (int)(100 * _mana) / _maxMana;
    }
    public void SetRageValue(float _rage)
    {
        if (_rage > 100f) slider.value = 100;
        slider.value = (int)_rage;
    }
}
