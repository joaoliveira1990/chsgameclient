﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public float cameraVelocity = 20.0f;
    public int zoomMin = 5;
    public int zoomMax = 25;

    private bool cameraSnapCenter = true;
    private float distanceTraveled = 0f;
    private Transform playerTransform;

    public void Initialize(GameObject player)
    {
        this.playerTransform = player.transform;

        Vector3 zoomedPosition = this.playerTransform.position;
        zoomedPosition.y = this.zoomMax;
        this.transform.position = zoomedPosition;
        CenterCameraOnPlayer();
    }

    private void Update()
    {
        this.distanceTraveled = Time.deltaTime * this.cameraVelocity;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            this.cameraSnapCenter = true;
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            this.cameraSnapCenter = false;
        }

        if (this.cameraSnapCenter)
        {
            CenterCameraOnPlayer();
        }

        ZoomCamera();

        if (Input.GetKeyDown(KeyCode.Y))
        {
            this.cameraSnapCenter = !this.cameraSnapCenter;
        }

        if (this.cameraSnapCenter == false)
        {
            CheckMousePositionOnScreen();
        }

    }

    private void ZoomCamera()
    {
        int mouseWheel = (int)Input.mouseScrollDelta.y;

        if (mouseWheel != 0)
        {
            if (mouseWheel == 1 && this.transform.position.y > this.zoomMin)
            {
                this.transform.Translate(Vector3.forward * this.distanceTraveled * 5);
            }
            else if (mouseWheel == -1 && this.transform.position.y < this.zoomMax)
            {
                this.transform.Translate(Vector3.back * this.distanceTraveled * 5);
            }
        }
    }

    private void CenterCameraOnPlayer()
    {
        float angleX = this.transform.localEulerAngles.x * Mathf.Deg2Rad;
        float angleY = this.transform.localEulerAngles.y * Mathf.Deg2Rad;
        float height = this.transform.position.y;

        float offX = height * Mathf.Sin(angleY) / Mathf.Tan(angleX);
        float offZ = height * Mathf.Cos(angleY) / Mathf.Tan(angleX);

        Vector3 off = new Vector3(-offX, height, -offZ);

        Vector3 snappedPos = this.playerTransform.position + off;
        snappedPos.y = height;
        this.transform.position = snappedPos;
    }

    private void CheckMousePositionOnScreen()
    {
        Vector2 mousePosition = Input.mousePosition;

        if (mousePosition.x < 20)
        {
            this.transform.Translate(Vector3.left * this.distanceTraveled);
        }
        else if (mousePosition.x > Screen.width - 20)
        {
            this.transform.Translate(Vector3.right * this.distanceTraveled);
        }

        if (mousePosition.y < 20)
        {
            float angleY = this.transform.localEulerAngles.y * Mathf.Deg2Rad;
            Vector3 trans = Vector3.back * Mathf.Cos(angleY) + Vector3.left * Mathf.Sin(angleY);
            this.transform.Translate(trans * this.distanceTraveled, Space.World);
        }
        else if (mousePosition.y > Screen.height - 20)
        {
            float angleY = this.transform.localEulerAngles.y * Mathf.Deg2Rad;
            Vector3 trans = Vector3.forward * Mathf.Cos(angleY) + Vector3.right * Mathf.Sin(angleY);
            this.transform.Translate(trans * this.distanceTraveled, Space.World);
        }
    }
    private void Look()
    {
        //float _mouseVertical = -Input.GetAxis("Mouse Y");
        //float _mouseHorizontal = Input.GetAxis("Mouse X");

        //verticalRotation += _mouseVertical * sensitivity * Time.deltaTime;
        //horizontalRotation += _mouseHorizontal * sensitivity * Time.deltaTime;

        //verticalRotation = Mathf.Clamp(verticalRotation, -clampAngle, clampAngle);

        //transform.localRotation = Quaternion.Euler(verticalRotation, 0f, 0f);
        //playerTransform.rotation = Quaternion.Euler(0f, horizontalRotation, 0f);
    }

    private void ToggleCursorMode()
    {
        Cursor.visible = !Cursor.visible;

        if (Cursor.lockState == CursorLockMode.None)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
