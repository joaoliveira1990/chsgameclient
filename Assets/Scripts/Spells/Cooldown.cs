﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cooldown : MonoBehaviour
{
    private float cooldown;
    private Image imageCooldown;
    private bool canUpdate = false;
    public void Initialize(float _cooldown)
    {
        imageCooldown = gameObject.GetComponent<Image>();
        cooldown = _cooldown;
        canUpdate = true;
    }

    void Update()
    {
        if (canUpdate)
        {
            imageCooldown.fillAmount += 1 / cooldown * Time.deltaTime;
            if (imageCooldown.fillAmount == 1f)
            {
                imageCooldown.fillAmount = 0f;
                canUpdate = false;
            }
        }
    }
}
