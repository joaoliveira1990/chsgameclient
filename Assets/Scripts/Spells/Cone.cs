﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cone : MonoBehaviour
{
    private float destroyAfter;
    public GameObject coneModel;
    public void Initialize(float _destroyAfter)
    {
        this.destroyAfter = _destroyAfter;
        StartCoroutine(DestroyAfter());
    }

    private IEnumerator DestroyAfter()
    {
        yield return new WaitForSeconds(this.destroyAfter);
        StopCoroutine("DestroyAfter");
        Destroy(this.gameObject);
    }
}
