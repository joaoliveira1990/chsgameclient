﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aoe : MonoBehaviour
{
    private float radius;
    private float destroyAfter;

    public GameObject aoeModel;
    public void Initialize(float _radius, float _destroyAfter)
    {
        this.radius = _radius;
        this.destroyAfter = _destroyAfter;

        aoeModel.transform.localScale += new Vector3(aoeModel.transform.localScale.x * _radius, 1f, aoeModel.transform.localScale.z * _radius);
        StartCoroutine(DestroyAfter());
    }

    private IEnumerator DestroyAfter()
    {
        yield return new WaitForSeconds(this.destroyAfter);
        StopCoroutine("DestroyAfter");
        Destroy(this.gameObject);
    }
}
