﻿    using System;
    public class UserInfo
    {
        public string _id { get; set; }
        public string email { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public int __v { get; set; }
    }

    public class UserLoggedIn
    {
        public UserInfo user { get; set; }
        public string token { get; set; }
        public int token_expires_in { get; set; }
    }
