﻿using System.Collections.Generic;
public class Error
{
    public string location { get; set; }
    public string param { get; set; }
    public string msg { get; set; }
}

public class ErrorList
{
    public List<Error> errors { get; set; }
}