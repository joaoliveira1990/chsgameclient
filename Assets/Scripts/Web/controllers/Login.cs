﻿using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine;
using DuloGames.UI;

public class Login : MonoBehaviour
{
    public InputField tfLogin, tfPassword;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void LoginUser()
    {
        if (!string.IsNullOrEmpty(tfLogin.text) && !string.IsNullOrEmpty(tfPassword.text))
        {
            StartCoroutine(SendLoginData());
        }
    }

    private IEnumerator SendLoginData()
    {
        var user = new User
        {
            email = tfLogin.text,
            password = tfPassword.text
        };
        // Delete cookie before requesting a new login
        HttpRequests.CookieString = null;

        var request = HttpRequests.Post("api/auth/login", JsonUtility.ToJson(user));
        yield return request.Send();

        if (request.isNetworkError)
        {
            Debug.Log("ERROR");
            Debug.LogError(request.error);
        }
        else
        {
            Debug.Log("OK");
            HttpRequests.CookieString = request.GetResponseHeader("set-cookie");
            Debug.Log(HttpRequests.CookieString);
            Debug.Log(request.downloadHandler.text);
            
            try
            {
                ErrorList json = JsonUtility.FromJson<ErrorList>(request.downloadHandler.text);
                //show error
                Debug.Log(ObjectDumper.Dump(json));
            }
            catch(Exception e)
            {
                Debug.Log($"NO ERRORS {e}");
            }

            try
            {
                UserLoggedIn json = JsonUtility.FromJson<UserLoggedIn>(request.downloadHandler.text);
                Debug.Log(ObjectDumper.Dump(json));
                this.GetComponent<UILoadScene>().LoadScene();
            }
            catch(Exception e)
            {
                Debug.Log($"ERRORS {e}");
            }
        }
    }
}
