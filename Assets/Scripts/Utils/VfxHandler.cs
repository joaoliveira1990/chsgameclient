﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VfxHandler : MonoBehaviour
{
    void Start()
    {
        Destroy(this.gameObject, GetComponent<ParticleSystem>().main.duration);
    }
}
