﻿public enum PlayerState
{
    idle,
    chase,
    attack
}

public enum AllEffects
{
    overpower = 0,
    holyFire,
    renew,
    counterspell,
    combustion,
    stun,
    avatar,
    deathImmunity,
    damageReduction,
    slow
}

public enum Classes
{
    warrior = 1,
    mage,
    priest,
    hunter,
    paladin,
    deathknight,
    warlock,
    druid,
    rogue,
    shaman
}