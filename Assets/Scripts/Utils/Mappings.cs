﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Mappings
{
    public static Color ClassToColor(int _selectedClass)
    {
        switch (_selectedClass)
        {
            case (int)Classes.warrior:
                return Color.red;
            case (int)Classes.mage:
                return Color.cyan;
            case (int)Classes.priest:
                return Color.cyan;
            default:
                return Color.grey;
        }
    }

    public static Color TeamToColor(int _team)
    {
        if (_team == 1) return new Color(255, 0, 0);
        else return new Color(0, 0, 255);
    }

    public static int InputToNumber(string _input)
    {
        switch (_input)
        {
            case "1":
                return 0;
            case "2":
                return 1;
            case "3":
                return 2;
            case "4":
                return 3;
            case "5":
                return 4;
            case "6":
                return 5;
            case "7":
                return 6;
            case "8":
                return 7;
            case "9":
                return 8;
            default:
                return -1;
        }
    }
}
