﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientSend : MonoBehaviour
{
    private static void SendTCPData(Packet _packet)
    {
        _packet.WriteLength();
        Client.instance.tcp.SendData(_packet);
    }

    private static void SendUDPData(Packet _packet)
    {
        _packet.WriteLength();
        Client.instance.udp.SendData(_packet);
    }

    #region Packets
    public static void WelcomeReceived()
    {
        /*
        using(Packet _packet = new Packet((int)ClientPackets.welcomeReceived)) {
            _packet.Write(Client.instance.myId);
            _packet.Write(UIManager.instance.usernameField.text);
            _packet.Write(UIManager.instance.gameManager.selectedChampion);
            _packet.Write(UIManager.instance.gameManager.team);

            SendTCPData(_packet);
        }
        */
        using (Packet _packet = new Packet((int)ClientPackets.welcomeReceived))
        {
            _packet.Write(Client.instance.myId);
            _packet.Write(UIManager.instance.usernameField.text);
            _packet.Write(UIManager.instance.gameManager.selectedClass);
            _packet.Write(UIManager.instance.gameManager.selectedSpec);
            _packet.Write(UIManager.instance.gameManager.team);

            SendTCPData(_packet);
        }
    }


    public static void PlayerAction(Vector3 _newPosition)
    {
        using (Packet _packet = new Packet((int)ClientPackets.playerAction))
        {
            _packet.Write(_newPosition);
            _packet.Write(GameManager.instance.mouseMode);

            //_packet.Write(GameManager.players[Client.instance.myId].transform.rotation);
            SendUDPData(_packet);
        }
    }

    /*
    public static void PlayerMovement(bool[] _inputs)
    {

        
         //To smooth out the movement you need to interpolate between the position updates that 
         //clients receive from the server. 
         //Vector3.Lerp is very useful for this, and although I don't cover it in this series, 
         //there's some code examples in the Discord server


        using (Packet _packet = new Packet((int)ClientPackets.playerMovement))
        {
            _packet.Write(_inputs.Length);
            foreach (bool _input in _inputs)
            {
                _packet.Write(_input);
            }
            _packet.Write(GameManager.players[Client.instance.myId].transform.rotation);
            SendUDPData(_packet);
        }
    }
    */


    public static void PlayerShoot(Vector3 _facing)
    {
        using (Packet _packet = new Packet((int)ClientPackets.playerShoot))
        {
            _packet.Write(_facing);

            SendTCPData(_packet);
        }
    }

    public static void PlayerThrowItem(Vector3 _facing)
    {
        using (Packet _packet = new Packet((int)ClientPackets.playerThrowItem))
        {
            _packet.Write(_facing);

            SendTCPData(_packet);
        }
    }

    public static void TriggerAbility(int _id, int _spellId, Vector3 _mouseTarget)
    {
        using (Packet _packet = new Packet((int)ClientPackets.triggerAbility))
        {
            _packet.Write(_id);
            _packet.Write(_spellId);
            _packet.Write(_mouseTarget);
            _packet.Write(GameManager.instance.mouseMode);

            SendTCPData(_packet);
        }
    }

    public static void BuyItem(int _id, int _itemId)
    {
        using (Packet _packet = new Packet((int) ClientPackets.buyItem))
        {
            _packet.Write(_id);
            _packet.Write(_itemId);

            SendTCPData(_packet);
        }
    }

     public static void AddTalent(int _id, int _spellId)
    {
        using (Packet _packet = new Packet((int) ClientPackets.addTalent))
        {
            _packet.Write(_id);
            _packet.Write(_spellId);

            SendTCPData(_packet);
        }
    }
    #endregion
}
