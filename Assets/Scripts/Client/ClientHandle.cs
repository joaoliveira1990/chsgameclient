﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class ClientHandle : MonoBehaviour
{
    public static void Welcome(Packet _packet)
    {
        string _msg = _packet.ReadString();
        int _myId = _packet.ReadInt();

        Client.instance.myId = _myId;
        ClientSend.WelcomeReceived();

        Client.instance.udp.Connect(((IPEndPoint)Client.instance.tcp.socket.Client.LocalEndPoint).Port);
    }

    #region PLAYER
    public static void SpawnPlayer(Packet _packet)
    {
        int _id = _packet.ReadInt();
        string _username = _packet.ReadString();
        int _selectedClass = _packet.ReadInt();
        int _selectedSpec = _packet.ReadInt();
        int _team = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        Quaternion _rotation = _packet.ReadQuaternion();
        float _health = _packet.ReadFloat();
        float _mana = _packet.ReadFloat();
        int _gold = _packet.ReadInt();

        GameManager.instance.SpawnPlayer(_id, _username, _selectedClass, _selectedSpec, _team, _position, _rotation, _health, _mana, _gold);
    }

    public static void PlayerConfiguration(Packet _packet)
    {
        int _id = _packet.ReadInt();
        int _selectedChampion = _packet.ReadInt();

        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            _player.SetSelectedChampion(_selectedChampion);
        }
    }

    public static void PlayerPath(Packet _packet)
    {
        int _packetLength = _packet.ReadInt();
        int _id = _packet.ReadInt();

        Vector3[] waypoints = new Vector3[_packetLength];

        for (int i = 0; i < _packetLength; i++)
        {
            waypoints[i] = _packet.ReadVector3();
        }

        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            _player.UpdatePlayerPath(waypoints);
        }
    }

    public static void EnemyPath(Packet _packet)
    {
        bool _isStopped = _packet.ReadBool();
        int _packetLength = _packet.ReadInt();
        int _id = _packet.ReadInt();

        Vector3[] waypoints = new Vector3[_packetLength];

        for (int i = 0; i < _packetLength; i++)
        {
            waypoints[i] = _packet.ReadVector3();
        }

        if (GameManager.enemies.TryGetValue(_id, out EnemyManager _enemy))
        {
            _enemy.UpdateEnemyPath(waypoints, _isStopped);
        }
    }

    public static void PlayerPosition(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Vector3 _playerPosition = _packet.ReadVector3();
        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            _player.PositionServerUpdate(_playerPosition);
        }
    }

    public static void PlayerWarp(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Vector3 _playerPosition = _packet.ReadVector3();
        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            _player.PositionWarp(_playerPosition);
        }
    }

    public static void PlayerRotation(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Quaternion _rotation = _packet.ReadQuaternion();
        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            _player.RotationServerUpdate(_rotation);
        }
    }

    public static void PlayerDisconnected(Packet _packet)
    {
        int _id = _packet.ReadInt();

        Destroy(GameManager.players[_id].gameObject);
        GameManager.players.Remove(_id);
    }

    public static void PlayerHealth(Packet _packet)
    {
        int _id = _packet.ReadInt();
        float _health = _packet.ReadFloat();
        float _maxHealth = _packet.ReadFloat();

        // TODO: Verify if is localplayerId and update healthbarvalue
        GameManager.instance.playerCamInstance.GetComponent<HudController>().SetHealthBarValue(_health, _maxHealth);
        GameManager.players[_id].SetHealth(_health);
    }

    public static void PlayerMana(Packet _packet)
    {
        int _id = _packet.ReadInt();
        float _mana = _packet.ReadFloat();
        float _maxMana = _packet.ReadFloat();

        // GameManager.players[_id].SetMana(_mana, _maxMana);
        GameManager.instance.playerCamInstance.GetComponent<HudController>().SetResourceBarValue(_mana, _maxMana);

    }

    public static void PlayerRage(Packet _packet)
    {
        int _id = _packet.ReadInt();
        float _rage = _packet.ReadFloat();

        // GameManager.players[_id].SetRage(_rage);
        GameManager.instance.playerCamInstance.GetComponent<HudController>().SetResourceBarValue(_rage, 100f);

    }

    public static void PlayerRespawned(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Vector3 _spawnPosition = _packet.ReadVector3();
        GameManager.players[_id].Respawn(_spawnPosition);
    }

    public static void PlayerState(Packet _packet)
    {
        int _playerId = _packet.ReadInt();
        int _state = _packet.ReadInt();
        int _targetId = _packet.ReadInt();
        string _targetTag = _packet.ReadString();

        if (GameManager.players.TryGetValue(_playerId, out PlayerManager _player))
        {
            _player.SetState(_state, _targetId, _targetTag);
        }
    }

    public static void PlayerAttacked(Packet _packet)
    {
        int _playerId = _packet.ReadInt();
        float _attackSpeed = _packet.ReadFloat();
        string _targetType = _packet.ReadString();
        int _targetId = _packet.ReadInt();
        string _anim = _packet.ReadString();
        int _vfxHitId = _packet.ReadInt();
        int _vfxCasterId = _packet.ReadInt();

        if (GameManager.players.TryGetValue(_playerId, out PlayerManager _casterPlayer))
        {
            // > Perform player caster animation 
            _casterPlayer.GetComponent<PlayerAnimator>().PlayAnimation(_anim);

            // > Perform vfx on target and caster
            switch (_targetType)
            {
                case "Enemy":
                    if (GameManager.enemies.TryGetValue(_targetId, out EnemyManager _targetEnemy))
                    {
                        VfxManager.instance.PlayerAttackedVfx(_vfxHitId, _targetEnemy.transform.position);
                        VfxManager.instance.PlayerAttackedVfx(_vfxCasterId, _casterPlayer.shootOrigin);
                    }
                    break;
                case "Player":
                    if (GameManager.players.TryGetValue(_targetId, out PlayerManager _targetPlayer))
                    {
                        VfxManager.instance.PlayerAttackedVfx(_vfxHitId, _targetPlayer.transform.position);
                        VfxManager.instance.PlayerAttackedVfx(_vfxCasterId, _casterPlayer.shootOrigin);
                    }
                    break;
                default:
                    break;
            }
        }


    }

    public static void PlayerCasted(Packet _packet)
    {
        int _playerId = _packet.ReadInt();
        float _castTime = _packet.ReadFloat();

        if (GameManager.players.TryGetValue(_playerId, out PlayerManager _player))
        {
            // TODO: Call instant cast animation
            // _player.SetCastbar(_castTime);
        }
    }

    public static void PlayerCastedStart(Packet _packet)
    {
        int _playerId = _packet.ReadInt();
        float _castTime = _packet.ReadFloat();
        string _anim = _packet.ReadString();

        if (GameManager.players.TryGetValue(_playerId, out PlayerManager _player))
        {
            _player.OnPlayerCasted(_castTime, _anim);
        }
    }

    public static void PlayerStopped(Packet _packet)
    {
        int _playerId = _packet.ReadInt();

        if (GameManager.players.TryGetValue(_playerId, out PlayerManager _player))
        {
            _player.GetComponent<PlayerManager>().StopAgent();
        }
    }

    /// <summary>Add vfx to target and indicator in HUD</summary>
    public static void PlayerAddEffect(Packet _packet)
    {
        int _playerId = _packet.ReadInt();
        float _lifeTime = _packet.ReadFloat();
        int _effectId = _packet.ReadInt();
        int _stacks = _packet.ReadInt();
        string _type = _packet.ReadString();

        if (_type == "player")
        {
            if(_playerId == GameManager.instance.localPlayerId) 
            {
                GameManager.instance.playerCamInstance.GetComponent<HudController>().AddEffect(_effectId, _lifeTime, _stacks);
            }

            if (GameManager.players.TryGetValue(_playerId, out PlayerManager _targetPlayer))
            {
                VfxManager.instance.PlayerAddEffectVfx(_effectId, _targetPlayer.transform, _lifeTime, _targetPlayer);
            }   
        }
        else if(_type == "enemy")
        {
            if (GameManager.enemies.TryGetValue(_playerId, out EnemyManager _targetEnemy))
            {
                VfxManager.instance.EnemyAddEffectVfx(_effectId, _targetEnemy.transform, _lifeTime, _targetEnemy);
            }
        }
        //TODO: Add also effect in case it is an enemy
    }

    public static void PlayerRemoveEffect(Packet _packet)
    {
        int _playerId = _packet.ReadInt();
        int _effectId = _packet.ReadInt();

        if(_playerId == GameManager.instance.localPlayerId) {
          GameManager.instance.playerCamInstance.GetComponent<HudController>().RemoveEffect(_effectId);
        }

        if (GameManager.players.TryGetValue(_playerId, out PlayerManager _targetPlayer))
        {
          VfxManager.instance.RemoveEffect(_effectId,  _targetPlayer);
        }
        //TODO: Add also effect in case it is an enemy
    }

    public static void PlayerBoughtItem(Packet _packet)
    {
        int _playerId = _packet.ReadInt();
        int _itemId = _packet.ReadInt();
        int _gold = _packet.ReadInt();

        GameManager.instance.playerCamInstance.GetComponent<HudController>().EquipItem(_itemId, _gold);
    }
    public static void PlayerSetGold(Packet _packet)
    {
        int _playerId = _packet.ReadInt();
        int _gold = _packet.ReadInt();

        GameManager.instance.playerCamInstance.GetComponent<HudController>().SetGold(_gold);
    }
    public static void PlayerSetXpLevel(Packet _packet)
    {
        int _playerId = _packet.ReadInt();
        int _level = _packet.ReadInt();
        int _currentXp = _packet.ReadInt();
        int _xpRequiredToLevel = _packet.ReadInt();
        bool _isLevelUp = _packet.ReadBool();

        //UpdateUI
        if(_playerId == GameManager.instance.localPlayerId) {
          GameManager.instance.playerCamInstance.GetComponent<HudController>().SetXpLevel(_level, _currentXp, _xpRequiredToLevel);
        }
        //UpdateSharedUI
        GameManager.players[_playerId].SetXpLevel(_level, _currentXp, _isLevelUp);
    }
    public static void PlayerSetSpeed(Packet _packet)
    {
        int _playerId = _packet.ReadInt();
        float _speed = _packet.ReadFloat();

        GameManager.players[_playerId].SetAgentSpeed(_speed);
    }
    public static void PlayerIncreaseSizeForDuration(Packet _packet)
    {
        int _playerId = _packet.ReadInt();
        float _sizeMultiplier = _packet.ReadFloat();
        float _duration = _packet.ReadFloat();

        GameManager.players[_playerId].SetPlayerSize(_sizeMultiplier, _duration);
    }

    public static void ShowAbilityCooldown(Packet _packet)
    {
        int _playerId = _packet.ReadInt();
        float _cooldown = _packet.ReadFloat();
        int _hotbarIndex = _packet.ReadInt();

        GameManager.instance.playerCamInstance.GetComponent<HUDUpdater>().PlayButtonCooldown(_cooldown, _hotbarIndex);
    }
    public static void UpdateStatWindow(Packet _packet)
    {
        Stats playerStats = new Stats {
            strength = _packet.ReadInt(),
            intellect =  _packet.ReadInt(),
            agility =  _packet.ReadInt(),
            stamina =  _packet.ReadInt(),
            armor =  _packet.ReadInt(),
            magicResistance =  _packet.ReadInt(),
            healthPer5 =  _packet.ReadInt(),
            manaPer5 =  _packet.ReadInt(),
            critChance =  _packet.ReadFloat(),
            attackSpeed =  _packet.ReadFloat(),
            physicalDamage =  _packet.ReadFloat(),
            spellDamage =  _packet.ReadFloat()
        };

        if (GameManager.instance.isCameraSet())
        {
            GameManager.instance.playerCamInstance.GetComponent<HudController>().UpdateStats(playerStats);
        }
        else GameManager.instance.playerStats = playerStats;

        //GameManager.instance.playerCamInstance.GetComponent<HudController>().UpdateStats(playerStats);
    }


    #endregion

    #region WORLD
    public static void CreateItemSpawner(Packet _packet)
    {
        int _spawnerId = _packet.ReadInt();
        Vector3 _spawnerPosition = _packet.ReadVector3();
        bool _hasItem = _packet.ReadBool();

        GameManager.instance.CreateItemSpawner(_spawnerId, _spawnerPosition, _hasItem);
    }

    public static void ItemSpawned(Packet _packet)
    {
        int _spawnerId = _packet.ReadInt();

        GameManager.itemSpawners[_spawnerId].ItemSpawned();
    }

    public static void ItemPickedUp(Packet _packet)
    {
        int _spawnerId = _packet.ReadInt();
        int _byPlayer = _packet.ReadInt();

        GameManager.itemSpawners[_spawnerId].ItemPickedUp();
        GameManager.players[_byPlayer].itemCount++;
    }

    public static void SpawnAoe(Packet _packet)
    {
        Vector3 _spawnPosition = _packet.ReadVector3();
        float _radius = _packet.ReadFloat();
        float _destroyAfter = _packet.ReadFloat();
        int _aoeModel = _packet.ReadInt();
        int _parentId = _packet.ReadInt();
        string _parentType = _packet.ReadString();

        GameManager.instance.SpawnAoe(_spawnPosition, _radius, _destroyAfter, _aoeModel, _parentId, _parentType);
        //GameManager.players[_thrownByPlayer].itemCount--;
    }

    public static void ProjectilePosition(Packet _packet)
    {
        int _projectileId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();

        if (GameManager.projectiles.TryGetValue(_projectileId, out ProjectileManager _projectile))
        {
            _projectile.transform.position = _position;
        }

    }

    public static void ProjectileExploded(Packet _packet)
    {
        int _projectileId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();

        GameManager.projectiles[_projectileId].Explode(_position);
    }

    #endregion

    #region ENEMY
    public static void SpawnEnemy(Packet _packet)
    {
        int _enemyId = _packet.ReadInt();
        //string _enemyType = _packet.ReadString();
        int _enemyType = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        float _maxHealth = _packet.ReadFloat();
        GameManager.instance.SpawnEnemy(_enemyId, _enemyType, _position, _maxHealth);
    }

    public static void EnemyPosition(Packet _packet)
    {
        int _enemyId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();

        if (GameManager.enemies.TryGetValue(_enemyId, out EnemyManager _enemy))
        {
            // TODO: Have method on enemy prefab that updates (reconciliates) position (and also rotation, below)
            _enemy.transform.position = _position;
        }
    }

    public static void EnemyRotation(Packet _packet)
    {
        int _enemyId = _packet.ReadInt();
        Quaternion _rotation = _packet.ReadQuaternion();

        if (GameManager.enemies.TryGetValue(_enemyId, out EnemyManager _enemy))
        {
            _enemy.transform.rotation = _rotation;
        }
    }

    public static void EnemyState(Packet _packet)
    {
        int _enemyId = _packet.ReadInt();
        int _state = _packet.ReadInt();

        if (GameManager.enemies.TryGetValue(_enemyId, out EnemyManager _enemy))
        {
            _enemy.SetState(_state);
        }
    }

    public static void EnemyHealth(Packet _packet)
    {
        int _enemyId = _packet.ReadInt();
        float _health = _packet.ReadFloat();

        GameManager.enemies[_enemyId].SetHealth(_health);
    }

    public static void EnemyAttacked(Packet _packet)
    {
        int _enemyId = _packet.ReadInt();
        float _attackSpeed = _packet.ReadFloat();

        if (GameManager.enemies.TryGetValue(_enemyId, out EnemyManager _enemy))
        {
            _enemy.GetComponent<EnemyAnimator>().SetAnimationSpeed(_attackSpeed);
        }
    }

    public static void EnemyCastStarted(Packet _packet)
    {
        int _enemyId = _packet.ReadInt();
        float _castTime = _packet.ReadFloat();
        string _castAnimation = _packet.ReadString();

        if (GameManager.enemies.TryGetValue(_enemyId, out EnemyManager _enemy))
        {
            _enemy.OnEnemyCast(_castTime, _castAnimation);
        }
    }
    public static void EnemySetSpeed(Packet _packet)
    {
        int _enemyId = _packet.ReadInt();
        float _speed = _packet.ReadFloat();

        GameManager.enemies[_enemyId].SetAgentSpeed(_speed);
    }
    public static void SpawnProjectile(Packet _packet)
    {
        int _id = _packet.ReadInt();
        string _type = _packet.ReadString();
        Vector3 _positionCaster = _packet.ReadVector3();
        int _projectileId = _packet.ReadInt();
        Vector3 _positionTarget = _packet.ReadVector3();
        float _range = _packet.ReadFloat();
        int _team = _packet.ReadInt();

        GameManager.instance.SpawnProjectile(_id, _type, _positionCaster, _projectileId, _positionTarget, _range, _team);
    }

    #endregion

    #region EVENTS

    public static void ResetTimer(Packet _packet)
    {
        bool _toReset = _packet.ReadBool();

        if (_toReset)
        {
            int localPlayerId = GameManager.instance.localPlayerId;
            GameManager.instance.playerCamInstance.GetComponent<HUDUpdater>().ResetTimer();
        }
    }

    #endregion

    #region Debug

    public static void DrawBall(Packet _packet)
    {
        Vector3 position = _packet.ReadVector3();
        float radius = _packet.ReadFloat();
        string name = _packet.ReadString();

        //DrawPrimitives.instance.DrawBall(position);
    }

    #endregion

}
