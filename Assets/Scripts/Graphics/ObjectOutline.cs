﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectOutline : MonoBehaviour
{
    [SerializeField] private Material outlineMaterial;
    [SerializeField] private float outlineScaleFactor;
    [SerializeField] private Color outlineColor;
    private Renderer outlineRenderer;

    // Start is called before the first frame update
    void Start()
    {
        outlineRenderer = CreateOutline(this.outlineMaterial, this.outlineScaleFactor, this.outlineColor);
        outlineRenderer.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    Renderer CreateOutline(Material _outlineMat, float _scaleFactor, Color _color)
    {
        GameObject outlineObject = Instantiate(this.gameObject, this.transform.position, this.transform.rotation, this.transform);
        Renderer rend = outlineObject.GetComponent<Renderer>();

        rend.material = _outlineMat;
        rend.material.SetColor("_OutlineColor", _color);
        rend.material.SetFloat("_Scale", _scaleFactor);
        rend.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

        outlineObject.GetComponent<ObjectOutline>().enabled = false;
        outlineObject.GetComponent<Collider>().enabled = false;

        rend.enabled = false;

        return rend;
    }
}
