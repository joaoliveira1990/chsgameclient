﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VfxManager : MonoBehaviour
{
    public static VfxManager instance;

    [SerializeField] public GameObject[] vfxOnHit;
    [SerializeField] public EffectVfx[] vfxOnEffect;
    [SerializeField] private GameObject[] current;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }
    }

    public void PlayerAttackedVfx(int _id, Transform _transform)
    {
        Debug.Log(_id);
        if (IsOutOfBounds(this.vfxOnHit, _id)) return;
        Instantiate(this.vfxOnHit[_id], _transform);
    }

    public void PlayerAttackedVfx(int _id, Vector3 _position)
    {
        if (IsOutOfBounds(this.vfxOnHit, _id)) return;
        Instantiate(this.vfxOnHit[_id], _position, Quaternion.identity);
    }

    public void PlayerAddEffectVfx(int _effectId, Transform _transform, float _lifeTime, PlayerManager _targetPlayer)
    {
        if (IsOutOfBounds(this.vfxOnEffect, _effectId)) return;
        _targetPlayer.CleanEffects();

        EffectVfx existingEffect = _targetPlayer.GetEffectByName((AllEffects)_effectId);
        if (existingEffect != null) {
          Debug.Log("REMOVING ADD");
          Destroy(existingEffect.gameObject);
        }

        EffectVfx effectVfxInstance = Instantiate(this.vfxOnEffect[_effectId], _transform);
        _targetPlayer.AddEffect(effectVfxInstance);

        Destroy(effectVfxInstance.gameObject, _lifeTime);
    }
    public void EnemyAddEffectVfx(int _effectId, Transform _transform, float _lifeTime, EnemyManager _targetEnemy)
    {
        if (IsOutOfBounds(this.vfxOnEffect, _effectId)) return;
        _targetEnemy.CleanEffects();

        EffectVfx existingEffect = _targetEnemy.GetEffectByName((AllEffects)_effectId);
        if (existingEffect != null) {
          Debug.Log("REMOVING ADD");
          Destroy(existingEffect.gameObject);
        }

        EffectVfx effectVfxInstance = Instantiate(this.vfxOnEffect[_effectId], _transform);
        _targetEnemy.AddEffect(effectVfxInstance);

        Destroy(effectVfxInstance.gameObject, _lifeTime);
    }

    public void RemoveEffect(int _effectId, PlayerManager _targetPlayer)
    {
        if (IsOutOfBounds(this.vfxOnEffect, _effectId)) return;
        _targetPlayer.CleanEffects();

        EffectVfx existingEffect = _targetPlayer.GetEffectByName((AllEffects)_effectId);
        if (existingEffect != null){
          Debug.Log("REMOVING REMOVE");
          Debug.Log(existingEffect.id);
          Destroy(existingEffect.gameObject);
        }
    }

    private bool IsOutOfBounds(GameObject[] _array, int _id)
    {
        return _id < 0 || _id >= _array.Length;
    }

    private bool IsOutOfBounds(EffectVfx[] _array, int _id)
    {
        return _id < 0 || _id >= _array.Length;
    }
}
